# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_inliststring, d_string

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, pointCloudOutput
# #}
# add your imports before the next gnomon tag

from copy import deepcopy

import numpy as np

# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten

@algorithmPlugin(version="0.1.0", coreversion="1.0.0", name="Cell property operation")
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput("out_df", data_plugin="gnomonPointCloudDataPandas")
class cellPropertyOperation(gnomon.core.gnomonAbstractFormAlgorithm):
    """Create a new property as a basic combination of two existing ones

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['property_a'] = d_string("Property A", "", "Property to use as the first term of the operation")
        self._parameters['property_b'] = d_string("Property B", "", "Property to use as the second term of the operation")
        self._parameters['operation'] = d_inliststring("Operation", "ratio", ["sum", "difference", "product", "ratio"], "Type of operation to apply")
        self._parameters['result_name'] = d_string("Result Name", "", "Name to assign to the result property")

        self.df = {}
        self.out_df = {}

    def run(self):
        self.out_df = {}
        for time in self.df.keys():
            df = self.df[time]
            # #}
            # implement the run method

            out_df = deepcopy(df)

            print(df.columns)

            values_a = df[self['property_a']].values
            values_b = df[self['property_b']].values
            operation_symbols = {"sum": "+", "difference": "-", "product": "*", "ratio": "/"}
            operation_functions = {
                "sum": np.sum,
                "difference": np.diff,
                "product": np.prod,
                "ratio": lambda a, axis : np.exp(np.diff(np.log(a), axis=axis))
            }
            result = operation_functions[self['operation']]([values_b, values_a], axis=0)[0]

            operation_result_name = f"{self['property_a']}{operation_symbols[self['operation']]}{self['property_b']}"
            result_name = self['result_name'] if self['result_name'] != "" else operation_result_name

            out_df[result_name] = result

            self.out_df[time] = out_df
