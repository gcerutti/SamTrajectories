# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_inliststringlist, d_string

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, imageOutput
# #}
# add your imports before the next gnomon tag

from copy import deepcopy

import numpy as np

from timagetk import SpatialImage, MultiChannelImage

# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten

@algorithmPlugin(version="0.1.0", coreversion="1.0.0", name="Merge channels")
@imageInput("img", data_plugin="gnomonImageDataMultiChannelImage")
@imageOutput("merged_img", data_plugin="gnomonImageDataMultiChannelImage")
class mergeImageChannels(gnomon.core.gnomonAbstractFormAlgorithm):
    """Create a new channel by merging several other channels

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['channels'] = d_inliststringlist("Channels", [""],[""], "Channels on which to quantify signal intensity inside cells")
        self._parameters['result_channel'] = d_string("Result Channel", "", "Channel name to assign to the result image (may replace an existing one)")


        self.img = {}
        self.merged_img = {}

    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]

            self._parameters['channels'].setValues(img.channel_names)
            self._parameters['channels'].setValue(img.channel_names)

    def run(self):
        self.merged_img = {}
        for time in self.img.keys():
            img = self.img[time]
            # #}
            # implement the run method

            channels = [
                img.get_channel(c).get_array().astype(float)
                for c in self['channels']
            ]

            # TODO: Normalize channels before merge (adapthist)

            if len(channels) > 0:
                result = np.sum(channels, axis=0)
                result = np.minimum(result, np.iinfo(img.dtype).max)
                result = np.maximum(result, np.iinfo(img.dtype).min)
                result = result.astype(img.dtype)

                operation_result_channel = self['channels'][0] + "".join([f"_{c}" for c in self['channels'][1:]])
                result_name = self['result_channel'] if self['result_channel'] != "" else operation_result_channel

                img_dict = {c: deepcopy(img.get_channel(c)) for c in img.channel_names}
                result_img = SpatialImage(result, voxelsize=img.voxelsize)
                img_dict.update({result_name: result_img})

                self.merged_img[time] = MultiChannelImage(img_dict)
