# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_inliststring, d_string, d_int, d_real

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, imageInput, pointCloudOutput
# #}
# add your imports before the next gnomon tag

from copy import deepcopy

import numpy as np
from scipy.spatial.distance import cdist

from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.pointmatching import apply_trsf_to_points

# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten

@algorithmPlugin(version="0.1.0", coreversion="1.0.0", name="NucTracker")
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@pointCloudOutput('out_df', data_plugin="gnomonPointCloudDataPandas")
class nucleiTracking(gnomon.core.gnomonAbstractFormAlgorithm):
    """Perform point tracking on detected nuclei points. 

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['registration_channel'] = d_inliststring("Registration channel", "", [""], "Which channel to use to compute image transformations")

        self._parameters['registration_pyramid_lowest'] = d_int("Pyramid lowest", 2, 0, 4, "Lowest level at which to compute deformation.")
        self._parameters['registration_pyramid_highest'] = d_int("Pyramid highest", 5, 1, 5, "Highest level at which to compute deformation.")

        self._parameters['distance_threshold'] = d_real("Distance threshold",  4., 0, 15., 1, "Maximum distance allowed to match points (after registration)")

        self._parameter_groups = {}
        for parameter_name in ["registration_pyramid_lowest", "registration_pyramid_highest"]: #+ ["registration_block_size", "registration_elastic_sigma"]:
            self._parameter_groups[parameter_name] = 'registration'
            
        self.df = {}
        self.img = {}
        self.out_df = {}
        
    def refreshParameters(self):
        if len(self.img) > 0:
            img = list(self.img.values())[0]
            if img is not None:
                if len(img) == 1:
                    if 'registration_channel' in self._parameters.keys():
                        del self._parameters['registration_channel']
                else:
                    if 'registration_channel' not in self._parameters:
                        self._parameters['registration_channel'] = d_string("Registration Channel", "", "Which channel to use to compute image transformations")
                    registration_channel = self['registration_channel']
                    self._parameters['registration_channel'].setValues(list(img.keys()))
                    if registration_channel in img.keys():
                        self._parameters['registration_channel'].setValue(registration_channel)
                    else:
                        self._parameters['registration_channel'].setValue(list(img.keys())[0])

    def run(self):
        self.out_df = {}
        for time in self.df.keys():
            df = self.df[time]
            # #}
            # implement the run method

            self.out_df[time] = deepcopy(df)

        time_points = np.sort([t for t in self.df])
        for previous_time, next_time in zip(time_points[:-1], time_points[1:]):
            previous_img = self.img[previous_time].get_channel(self["registration_channel"])
            next_img = self.img[next_time].get_channel(self["registration_channel"])

            rigid_trsf = blockmatching(
                reference_image=previous_img,
                floating_image=next_img,
                method='rigid',
                pyramid_highest_level=self["registration_pyramid_highest"],
                pyramid_lowest_level=self["registration_pyramid_lowest"]
            )

            affine_trsf = blockmatching(
                reference_image=previous_img,
                floating_image=next_img,
                init_trsf=rigid_trsf,
                method='affine',
                pyramid_highest_level=self["registration_pyramid_highest"],
                pyramid_lowest_level=self["registration_pyramid_lowest"]
            )

            vectorfield_trsf = blockmatching(
                reference_image=previous_img,
                floating_image=next_img,
                init_trsf=affine_trsf,
                method='vectorfield',
                pyramid_highest_level=self["registration_pyramid_highest"]-2,
                pyramid_lowest_level=self["registration_pyramid_lowest"]-1
            )

            previous_df = self.out_df[previous_time]
            previous_points = dict(zip(
                previous_df['label'].values,
                previous_df[['center_'+dim for dim in 'xyz']].values
            ))

            registered_points = dict(zip(
                [n for n in previous_points],
                apply_trsf_to_points([previous_points[n] for n in previous_points], vectorfield_trsf)
            ))

            next_df = self.out_df[next_time]
            next_points = dict(zip(
                next_df['label'].values,
                next_df[['center_'+dim for dim in 'xyz']].values
            ))

            point_distances = cdist(
                [next_points[n] for n in next_points],
                [registered_points[n] for n in registered_points]
            )
            point_distances[point_distances > self['distance_threshold']] = np.nan
            lineage_dict = dict(zip(
                [n for n in next_points],
                [[n for n in previous_points][np.nanargmin(d)] if not np.all(np.isnan(d)) else 0 for d in point_distances]
            ))

            self.out_df[next_time]['ancestor'] = [lineage_dict[n] for n in self.out_df[next_time]['label'].values]

        self.out_df[time_points[0]]['ancestor'] = self.out_df[time_points[0]]['label'].values
