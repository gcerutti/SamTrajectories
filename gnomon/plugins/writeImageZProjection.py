# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_inliststringlist, d_inliststring, d_string

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, binaryImageInput
# #}
# add your imports before the next gnomon tag

from copy import deepcopy

import numpy as np

from timagetk import SpatialImage, MultiChannelImage
from timagetk.io import imsave

# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten

@algorithmPlugin(version="0.1.0", coreversion="1.0.0", name="Write Z-proj")
@imageInput("img", data_plugin="gnomonImageDataMultiChannelImage")
@binaryImageInput("binary_img", data_plugin="binaryImageDataSpatialImage")
class writeImageZProjection(gnomon.core.gnomonAbstractFormAlgorithm):
    """

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['path'] = d_string("Path", "", "Path to write the z-projection")
        self._parameters['projection_type'] = d_inliststring("Projection Type", "max", ['max', 'mean'], "Type of z-projection")
        self._parameters['altitude_path'] = d_string("Altitude Path", "", "Path to write the altitude map")
        self._parameters['channels'] = d_inliststringlist("Channels", [""],[""], "Channels to include in the z-projection")

        self.img = {}
        self.binary_img = {}

    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]

            self._parameters['channels'].setValues(img.channel_names)
            self._parameters['channels'].setValue(img.channel_names)

    def run(self):
        for time in self.img.keys():
            img = self.img[time]
            # #}
            # implement the run method

            print(self['channels'])

            if self['projection_type'] == 'max':
                projected_img = MultiChannelImage({
                    c: SpatialImage(img[c].get_array().max(axis=0), voxelsize=img.voxelsize[1:])
                    for c in self['channels']
                })
            elif self['projection_type'] == 'mean':
                projected_img = MultiChannelImage({
                    c: SpatialImage(img[c].get_array().mean(axis=0).astype(img[c].dtype), voxelsize=img.voxelsize[1:])
                    for c in self['channels']
                })
            imsave(self['path'], projected_img, force=True)

            if time in self.binary_img:
                binary_img = self.binary_img[time]

                z_range = np.arange(binary_img.shape[0]) # * binary_img.voxelsize[0]
                z_img = z_range[:, np.newaxis, np.newaxis] * np.ones(binary_img.shape)
                z_img[binary_img == 0] = np.nan
                projected_z_img = np.nanmean(z_img, axis=0)
                projected_z_img[np.isnan(projected_z_img)] = 0
                projected_z_img = np.round(projected_z_img).astype(np.uint16)
                projected_z_img = SpatialImage(projected_z_img, voxelsize=img.voxelsize[1:])

                imsave(self['altitude_path'], projected_z_img, force=True)

