# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
import pandas as pd
from dtkcore import d_inliststring, d_real

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, meshInput, pointCloudOutput, dataFrameOutput
# #}
# add your imports before the next gnomon tag

from copy import deepcopy

import numpy as np
import scipy.ndimage as nd

from timagetk.algorithms.pointmatching import pointmatching, apply_trsf_to_points

from cellcomplex.property_topomesh.analysis import compute_topomesh_vertex_property_from_faces
from cellcomplex.property_topomesh.segmentation import topomesh_vertex_property_watershed_segmentation, topomesh_merge_vertex_watershed_regions

# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Nuclei Outer Organs")
@pointCloudInput("nuclei_df", data_plugin="gnomonPointCloudDataPandas")
@meshInput('surface_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@pointCloudOutput("out_df", data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('data_df', data_plugin="gnomonDataFrameDataPandas")
class nucleiOuterOrganSegmentation(gnomon.core.gnomonAbstractFormAlgorithm):
    """Assign nuclei to outer organs based on surface mesh segmentation

    """

    def __init__(self):
        super().__init__()

        self._parameters = {
            'gaussian_sigma': d_real("Gaussian Sigma", 10., 0, 100, 2, "Standard deviation of the Gaussian kernel used for seed detection in the surface segmentation"),
            'merge_threshold': d_real("Merge Threshold", 0.015, 0., 1., 3, "Threshold used to merge regions in the surface segmentation"),
            'organ_distance': d_real("Organ Distance", 70, 40, 100, 1, "Radial distance above which primordia regions are considered outer organs"),
        }

        self.nuclei_df = {}
        self.surface_topomesh = {}
        self.out_df = {}
        self.data_df = {}

    def run(self):
        self.out_df = {}
        self.data_df = {}
        # #}
        # implement the run method

        property_name = 'principal_curvature_min'

        for time in self.nuclei_df.keys():
            nuclei_df = self.nuclei_df[time]
            out_df = nuclei_df.copy(deep=True)

            alignment_trsf = pointmatching(nuclei_df[[f"center_{dim}" for dim in 'xyz']], nuclei_df[[f"aligned_{dim}" for dim in 'xyz']])
            clv3_center = apply_trsf_to_points([[0, 0, 0]], alignment_trsf)[0]
            vertical_axis = apply_trsf_to_points([[0, 0, 1]], alignment_trsf)[0] - clv3_center

            l1_mask = nuclei_df['layer'].values == 1

            surface_topomesh = deepcopy(self.surface_topomesh[time])

            topomesh_vertex_property_watershed_segmentation(
                surface_topomesh,
                property_name,
                minima=False,
                labelchoice='most',
                weighting='uniform',
                gaussian_sigma=self['gaussian_sigma']
            )

            topomesh_merge_vertex_watershed_regions(
                surface_topomesh,
                property_name,
                minima=False,
                depth_threshold=self['merge_threshold'],
                engulfed_fraction=1.
            )

            compute_topomesh_vertex_property_from_faces(surface_topomesh, 'area', weighting='uniform')

            surface_vertices = np.array(list(surface_topomesh.wisps(0)))
            surface_points = surface_topomesh.wisp_property('barycenter', 0).values(surface_vertices)
            surface_vertex_areas = surface_topomesh.wisp_property('area', 0).values(surface_vertices)
            surface_regions = surface_topomesh.wisp_property(f'{property_name}_regions', 0).values(surface_vertices)
            regions = np.unique(surface_regions)

            region_centers = np.transpose(
                [nd.sum(surface_vertex_areas * surface_points[:, k], surface_regions, index=regions) for k in range(3)]
            )
            region_centers /= nd.sum(surface_vertex_areas, surface_regions, index=regions)[:, np.newaxis]

            region_vectors = region_centers - clv3_center
            region_vertical_vectors = np.dot(region_vectors, vertical_axis)[:, np.newaxis] * vertical_axis
            region_radial_vectors = region_vectors - region_vertical_vectors
            region_radial_distances = np.linalg.norm(region_radial_vectors, axis=1)

            organ_regions = regions[region_radial_distances > self['organ_distance']]
            surface_topomesh.update_wisp_property(
                'organ', 0,
                {v: int(r in organ_regions) for v, r in surface_topomesh.wisp_property(f'{property_name}_regions', 0).items()}
            )

            nuclei_points = out_df[l1_mask][[f"center_{dim}" for dim in 'xyz']].values
            nuclei_surface_distances = np.linalg.norm(nuclei_points[:, np.newaxis] - surface_points[np.newaxis], axis=-1)
            nuclei_surface_vertices = surface_vertices[np.nanargmin(nuclei_surface_distances, axis=1)]

            nuclei_region_ids = surface_topomesh.wisp_property(f'{property_name}_regions', 0).values(nuclei_surface_vertices)
            out_df['region_index'] = np.nan
            out_df.loc[l1_mask, 'region_index'] = nuclei_region_ids

            region_distances = dict(zip(regions, region_radial_distances))
            out_df['region_distance'] = np.nan
            out_df.loc[l1_mask, 'region_distance'] = [region_distances[r] for r in nuclei_region_ids]

            nuclei_organ = surface_topomesh.wisp_property('organ', 0).values(nuclei_surface_vertices)
            out_df['organ'] = np.nan
            out_df.loc[l1_mask, 'organ'] = nuclei_organ

            self.out_df[time] = out_df
            self.data_df[time] = deepcopy(out_df)
