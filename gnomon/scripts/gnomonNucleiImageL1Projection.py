import os
import importlib
import argparse

import gnomon.core
import gnomon.visualization

from dtkcore import array_real_2

from gnomon.utils.gnomonPlugin import load_plugin_group
from gnomon.utils.views import gnomonStandaloneVtkView, gnomonStandaloneMplView


def initialize_plugins():
    load_plugin_group('dataDictWriter')
    load_plugin_group('imageReader')
    load_plugin_group('imageFilter')
    load_plugin_group('imageRegistration')
    load_plugin_group('imageVtkVisualization')
    load_plugin_group('imageWriter')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--project-path', help='Path to the gnomon project directory', default="./")
    parser.add_argument('-f', '--filename', help='SAM image sequence identifier', required=True)
    parser.add_argument('-t', '--times', default=[0], nargs='+', help='List of individual time points to process', type=int)
    parser.add_argument('-m', '--microscopy-directory', help='Relative path to CZI image directory [default : microscopy/]', default=None)
    parser.add_argument('-r', '--results-directory', help='Relative path to output directory [default : results/]', default=None)
    parser.add_argument('-c', '--channel-names', help='List of chanel names in the same order as in the microscopy image', nargs='+', type=str, required=True)
    parser.add_argument('-mo', '--microscope-orientation', help='Upright or inverted orientation of the microscope', default="upright", choices=["upright", "inverted"])
    parser.add_argument('-R', '--registered', action='store_true', help='Whether to apply rigid registration before computing L1 slices')
    args = parser.parse_args()

    initialize_plugins()

    project_path = args.project_path # "/Users/gcerutti/Projects/RDP/HugoNucleiTrajectories/"
    filename = args.filename # "qDII CLV AHP6 PI SAM6"
    file_times = args.times # [0, 6]
    channel_names = args.channel_names # ["TagBFP", "CLV3", "AHP6", "DIIV"]
    microscope_orientation = args.microscope_orientation # "upright"

    microscopy_directory = args.microscopy_directory if args.microscopy_directory is not None else "microscopy/" #data/AHP6 exK1
    microscopy_dirname = f"{project_path}/{microscopy_directory}"

    reporter_names = [c for c in channel_names if c not in ['TagBFP', 'PI', 'PIN1', 'DIIV', 'CLV3']]
    membrane_names = [c for c in channel_names if c in ['PI', 'PIN1']]

    output_directory = args.results_directory if args.results_directory is not None else "results/"
    output_dirname = f"{project_path}/{output_directory}"
    if not os.path.exists(f"{output_dirname}/{filename}"):
        os.makedirs(f"{output_dirname}/{filename}")

    reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
    paths = [f"{microscopy_dirname}/{filename}-T{t:02d}.czi" for t in file_times]
    path = "".join([f",{p}" for p in paths])[1:]
    reader.setPath(path)
    reader.run()

    old_channel_names = reader.image()[0].channels() # ["TaYFP-T1", "TaCFP-T2", "EBFP2-T3", "mCher-T3", PI-T4"]

    rename = gnomon.core.imageFilter_pluginFactory().create("channelNamesEdit")
    rename.setInput(reader.image())
    rename.refreshParameters()
    for old_channel_name, channel_name in zip(old_channel_names, channel_names):
        rename.setParameter(old_channel_name, channel_name)
    rename.run()

    if not os.path.exists(f"{output_dirname}/{filename}/surfcut"):
        os.makedirs(f"{output_dirname}/{filename}/surfcut")
    channel_colormaps = {'TagBFP': '0RGB_blue', 'PI': 'gray', 'PIN1': '0RGB_green', 'CLV3': '0CMY_magenta', 'DIIV': '0CMY_yellow'}
    channel_colormaps.update({c: '0CMY_cyan' for c in reporter_names})

    view = gnomonStandaloneVtkView()
    for channel in channel_names:
        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(rename.output())
        visu.refreshParameters()
        visu.setParameter("channel", channel)
        visu.setParameter("colormap", channel_colormaps[channel])
        visu.setParameter("show_histogram", False)
        visu.setParameter("opacity", 0.2)
        visu.setParameter("contrast", 0.5)
        visu.update()
        visu.setVisible(True)

    for i_t, t in enumerate(file_times):
        view.setCurrentTime(i_t)
        view.updateBounds()
        view.setCameraXY(False, False)
        view.saveScreenshot(f"{output_dirname}/{filename}/surfcut/{filename}-T{t:02d}_image.png")

    plugin_name = "mergeImageChannels"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    merge = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    merge.setInputImage(rename.output())
    merge.refreshParameters()
    merge.setParameter("channels", ['TagBFP', 'CLV3'] + reporter_names)
    merge.setParameter("result_channel", "MERGE")
    merge.run()

    if args.registered:
        rigid_registration = gnomon.core.imageRegistration_pluginFactory().create('backwardRigidRegistration')
        rigid_registration.setImage(merge.outputImage())
        rigid_registration.refreshParameters()
        rigid_registration.setParameter("channel", "MERGE")
        rigid_registration.setParameter("pyramid_highest", 4)
        rigid_registration.setParameter("pyramid_lowest", 2)
        rigid_registration.run()

        if not os.path.exists(f"{output_dirname}/{filename}/registration"):
            os.makedirs(f"{output_dirname}/{filename}/registration")
        data_writer = gnomon.core.dataDictWriter_pluginFactory().create("gnomonDataDictWriterJson")
        data_dict = rigid_registration.outputTransformation()
        for i_t, (t, (g_t, d_d)) in enumerate(zip(file_times, data_dict.items())):
            data_writer.setDataDict({t: d_d})
            data_writer.setPath(f"{output_dirname}/{filename}/registration/{filename}-T{t:02d}-rigid_transform.json")
            data_writer.run()

        gnomon.core.formAlgorithm_pluginFactory().clear()
        plugin_name = "writeImageZProjection"
        plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
        importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

        writer = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
        image = rigid_registration.output()
        for i_t, (t, (g_t, i)) in enumerate(zip(file_times, image.items())):
            writer.setInputImage({t: i})
            writer.refreshParameters()
            writer.setParameter("channels", ['TagBFP', 'CLV3', 'DIIV'] + membrane_names + reporter_names)
            writer.setParameter("projection_type", 'mean')
            writer.setParameter("path", f"{output_dirname}/{filename}/registration/{filename}-T{t:02d}_registered_image_2D.tif")
            writer.run()

    gnomon.core.formAlgorithm_pluginFactory().clear()
    plugin_name = "imageErosionSurfCut"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    surfcut = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    surfcut.setInputImage(rigid_registration.output() if args.registered else merge.outputImage())
    surfcut.refreshParameters()
    surfcut.setParameter("channel_name", "MERGE")
    surfcut.setParameter("orientation", "up" if microscope_orientation=='upright' else "down")
    surfcut.setParameter("gaussian_sigma", 3.)
    surfcut.setParameter("top_erosion", 6)
    surfcut.setParameter("bottom_erosion", 14)
    surfcut.run()

    view = gnomonStandaloneVtkView()
    for channel in channel_names:
        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(surfcut.outputImage())
        visu.refreshParameters()
        visu.setParameter("channel", channel)
        visu.setParameter("colormap", channel_colormaps[channel])
        visu.setParameter("show_histogram", False)
        visu.setParameter("opacity", 0.5)
        visu.setParameter("contrast", 0.5)
        visu.update()
        visu.setVisible(True)

    for i_t, t in enumerate(file_times):
        view.setCurrentTime(i_t)
        view.updateBounds()
        view.setCameraXY(False, False)
        view.saveScreenshot(f"{output_dirname}/{filename}/surfcut/{filename}-T{t:02d}{'_registered' if args.registered else ''}_surfcut_image.png")

    gnomon.core.formAlgorithm_pluginFactory().clear()
    plugin_name = "writeImageZProjection"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    writer = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    image = surfcut.outputImage()
    mask = surfcut.outputBinaryImage()
    for i_t, (t, (g_t, i), (_, b)) in enumerate(zip(file_times, image.items(), mask.items())):
        writer.setInputImage({t: i})
        writer.setInputBinaryImage({t: b})
        writer.refreshParameters()
        writer.setParameter("channels", ['TagBFP', 'CLV3', 'DIIV'] + membrane_names + reporter_names)
        writer.setParameter("path", f"{output_dirname}/{filename}/surfcut/{filename}-T{t:02d}{'_registered' if args.registered else ''}_surfcut_image_2D.tif")
        writer.setParameter("altitude_path", f"{output_dirname}/{filename}/surfcut/{filename}-T{t:02d}{'_registered' if args.registered else ''}_altitude_2D.tif")
        writer.run()
