from typing import List, Literal
import logging
from tqdm import tqdm

import numpy as np
import pandas as pd


def _primordium_max_angle(p, opening_angle):
    return (opening_angle+10*(np.maximum(0, 1-p)))


def _primordium_radius_min(p, clv3_radius):
    return (0.85 + 0.2*p + 0.15*np.maximum(0, -p))*clv3_radius


def _primordium_radius_max(p, clv3_radius):
    return (1.65 + 0.25*p + 0.1*np.maximum(0, -p))*clv3_radius


def define_primordia_regions_sectors(
    trajectory_df: pd.DataFrame, clv3_radius: float=28., opening_angle: float=35.,
    plastochron: float=12., primordium_range=range(-3, 3),
    min_radius_function=_primordium_radius_min,
    max_radius_function=_primordium_radius_max,
    max_angle_function=_primordium_max_angle
):
    """Define primordia by constructing geometrical sectors.

     The function defines primordia as geometrical sectors around predicted
     primordia positions (according to phyllotactic rules), selects
     trajectories within these sectors  and assigns them the rank of the
     primordium in the 'primordium_region' column.

    Parameters
    ----------
    trajectory_df
        The DataFrame containing SAM trajectories on which to construct the primordia mask
    clv3_radius
        The radius of the central zone (in micrometers)
    opening_angle
        The value of the opening angle from each sector (in degrees)
    plastochron
        The value of the plastochron (in hours)
    primordium_range
        The range of primordia for which to build masks
    min_radius_function
        A function returning the min radius of the sector given the primordium stage and clv3_radius
    max_radius_function
        A function returning the max radius of the sector given the primordium stage and clv3_radius
    max_angle_function
        A function returning the max angle of the sector given the primordium stage and opening_angle

    Returns
    -------
    None

    Notes
    -----
    The DataFrame is updated with primordium_region information

    """
    golden_angle = (2.*np.pi)/((np.sqrt(5)+1)/2.+1)

    trajectory_labels = np.unique(trajectory_df['trajectory_label'])
    trajectory_developmental_time = trajectory_df['hour_time'].values/plastochron - 0.5
    trajectory_df['primordium_region'] = np.nan

    for primordium in primordium_range:
        primordium_theta = (primordium*np.degrees(golden_angle) + 180)%360 - 180
        trajectory_primordium_time = primordium + trajectory_developmental_time

        max_angle = max_angle_function(trajectory_primordium_time, opening_angle)
        radius_min = min_radius_function(trajectory_primordium_time, clv3_radius)
        radius_max = max_radius_function(trajectory_primordium_time, clv3_radius)

        nuclei_primordium_mask = np.abs((trajectory_df['aligned_theta']-primordium_theta+180)%360 - 180) < max_angle/2
        nuclei_primordium_mask &= trajectory_df['radial_distance'] < radius_max
        nuclei_primordium_mask &= trajectory_df['radial_distance'] > radius_min

        trajectory_primordium_mask = dict(zip(
            trajectory_labels,
            [np.mean(nuclei_primordium_mask[trajectory_df["trajectory_label"].values==t]) > 0.5 for t in trajectory_labels]
        ))
        nuclei_primordium_mask = np.array([trajectory_primordium_mask[l] for l in trajectory_df["trajectory_label"].values])
        trajectory_df.loc[nuclei_primordium_mask, 'primordium_region'] = primordium

    trajectory_df['primordium_developmental_time'] = trajectory_df['primordium_region'] + trajectory_df['hour_time']/plastochron

    return trajectory_df


def define_primordia_regions_signal_maxima_trajectory(
    trajectory_df: pd.DataFrame, maxima_df: pd.DataFrame,
    signal_name: str='AHP6', extremum_type: str='maximum',
    primordium_range: List[int]=range(-3, 3),
    method: Literal['radius', 'top_k']='radius',
    primordium_radius: float=10., k: int=20,
    sorting: Literal['distance', 'intensity']='distance',
    plastochron=12.
):
    """Define primordia by selecting trajectories local signal maxima.

    The function selects trajectories around the signal maxima trajectories either
    within a radius around the local maxima or by selecting the k closest
    trajectories (in terms of radial distance or signal intensity) and assigns
    them the rank of the primordium in the 'primordium_region' column.

    It then computes mean values relative to signal maxima (mean relative radial
    distance, mean relative theta, mean relative signal intensity) for each
    primordium-associated trajectory.

    Parameters
    ----------
    trajectory_df
        The DataFrame containing SAM trajectories on which to define primordia
    maxima_df
        The DataFrame containing trajectories of signal maxima
    signal_name
        The name of the considered signal
    extremum_type
        Whether the considered points are maxima or minima of the signal
    primordium_range
       The range of primordia to define
    method
        Whether to select within a radius or the top-k closest trajectories
    primordium_radius
        Distance to the signal maximum below which trajectories are attributed to the corresponding primordium
    k
        The number of trajectories to attribute per primordia
    sorting
        How to sort the trajectories for each primordium:
          * 'distance' sorts the trajectories from the lowest relative radial distance (to signal maxima) to the highest;
          * 'intensity' sorts the signal maxima neighbouring trajectories from the highest signal intensity to the lowest;
    plastochron
        The value of the plastochron (in hours)

    Returns
    -------
    None

    Notes
    -----
    The DataFrame is updated with primordia information and relative values to signal maxima

    """

    try:
        assert method in ['radius', 'top_k']
    except AssertionError:
        raise(KeyError("The method should be either 'radius' or 'top_k'!"))

    if method == 'top_k':
        try:
            assert sorting in ['distance', 'intensity']
        except AssertionError:
            raise(KeyError("The sorting should be either 'distance' or 'intensity'!"))


    trajectory_labels = np.unique(trajectory_df['trajectory_label'])
    trajectory_df['primordium_region'] = np.nan

    hour_times = np.unique(trajectory_df['hour_time'])

    for primordium in tqdm(primordium_range, unit='primordium', desc=f"Attributing trajectories to primordia"):
        primordium_maxima_df = maxima_df.query(f"primordium=={primordium} & extremum_type=='{extremum_type}'")
        maxima_positions = np.array([
            primordium_maxima_df.query(f"hour_time=={time}")[['aligned_x', 'aligned_y']].values[0]
            if time in primordium_maxima_df["hour_time"].values
            else [np.nan, np.nan]
            for time in hour_times
        ])

        trajectory_distances = {}
        trajectory_relative_values = {c: {} for c in ['radial_distance', 'aligned_theta', signal_name]}

        maxima_values = {}
        for c in trajectory_relative_values:
            maxima_values[c] = np.array([
                primordium_maxima_df.query(f"hour_time=={time}")[c].values[0]
                if time in primordium_maxima_df["hour_time"].values
                else np.nan
                for time in hour_times
            ])

        for t in trajectory_labels:
            t_df = trajectory_df.query(f"trajectory_label=={t}")
            trajectory_maxima_distances = np.linalg.norm(
                t_df[['aligned_x', 'aligned_y']].values - maxima_positions,
                axis=-1
            )
            trajectory_distances[t] = np.nanmean(trajectory_maxima_distances)

            for c in trajectory_relative_values:
                trajectory_maxima_values = t_df[c].values - maxima_values[c]
                if c == 'aligned_theta':
                    trajectory_maxima_values = (trajectory_maxima_values + 180)%360 -180
                trajectory_relative_values[c][t] = np.nanmean(trajectory_maxima_values)

        if method == 'radius':
            trajectory_primordium_mask = {
                t: trajectory_distances[t] < primordium_radius
                for t in trajectory_labels
            }
            nuclei_primordium_mask = np.array([trajectory_primordium_mask[l] for l in trajectory_df["trajectory_label"].values])
        elif method == 'top_k':
            trajectory_distance_array = np.array([trajectory_distances[t] for t in trajectory_labels])
            if sorting=='distance':
                sorted_trajectories = np.array(trajectory_labels)[np.argsort(trajectory_distance_array)]
                top_k_trajectories = sorted_trajectories[:k]
            elif sorting=='intensity':
                # keep only trajectories within a radius before selecting on intensity
                neighboring_trajectories = np.array(trajectory_labels)[trajectory_distance_array < primordium_radius]
                trajectory_signal_intensity_array = [abs(trajectory_relative_values[signal_name][i]) for i in neighboring_trajectories]
                sorted_intensity_trajectories = np.array(neighboring_trajectories)[np.argsort(trajectory_signal_intensity_array)]
                top_k_trajectories = sorted_intensity_trajectories[:k]
            nuclei_primordium_mask = np.array([l in top_k_trajectories for l in trajectory_df["trajectory_label"].values])

        trajectory_df.loc[nuclei_primordium_mask, 'primordium_region'] = primordium

        for c in trajectory_relative_values:
            nuclei_primordium_relative_values = np.array([trajectory_relative_values[c][l] for l in trajectory_df["trajectory_label"].values])
            trajectory_df.loc[nuclei_primordium_mask, f'primordium_relative_{c}'] = nuclei_primordium_relative_values[nuclei_primordium_mask]

    trajectory_df['primordium_developmental_time'] = trajectory_df['primordium_region'] + trajectory_df['hour_time']/plastochron
