# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_inliststring, d_string, d_int, d_real, d_bool

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, imageInput, dataFrameOutput, pointCloudOutput
# #}
# add your imports before the next gnomon tag

from copy import deepcopy

import numpy as np
import pandas as pd
import networkx as nx
from scipy.spatial.distance import cdist

from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.trsf import compose_trsf, inv_trsf
from timagetk.algorithms.pointmatching import apply_trsf_to_points


# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten

@algorithmPlugin(version="0.1.0", coreversion="1.0.0", name="NucTracker Trajectories")
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@pointCloudOutput('out_df', data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('trajectory_df', data_plugin="gnomonDataFrameDataPandas")
class nucleiTrajectories(gnomon.core.gnomonAbstractFormAlgorithm):
    """Perform point tracking on detected nuclei points. 

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['registration_channel'] = d_inliststring("Registration channel", "", [""], "Which channel to use to compute image transformations")

        self._parameters['registration_pyramid_lowest'] = d_int("Pyramid lowest", 2, 0, 4, "Lowest level at which to compute deformation.")
        self._parameters['registration_pyramid_highest'] = d_int("Pyramid highest", 5, 1, 5, "Highest level at which to compute deformation.")

        # distance_threshold = 3.75 # 4.5 # 5.25
        self._parameters['distance_threshold'] = d_real("Distance threshold",  3.75, 0, 15., 2, "Maximum distance allowed to match points (after registration)")
        self._parameters['include_incomplete'] = d_bool("Include incomplete trajectories", True, "Whether to include trajectories that do not start at the 1st time point and end at the last")

        self._parameter_groups = {}
        for parameter_name in ["registration_pyramid_lowest", "registration_pyramid_highest"]: #+ ["registration_block_size", "registration_elastic_sigma"]:
            self._parameter_groups[parameter_name] = 'registration'
            
        self.df = {}
        self.img = {}
        self.out_df = {}
        self.trajectory_df = {}
        
    def refreshParameters(self):
        if len(self.img) > 0:
            img = list(self.img.values())[0]
            if img is not None:
                if len(img) == 1:
                    if 'registration_channel' in self._parameters.keys():
                        del self._parameters['registration_channel']
                else:
                    if 'registration_channel' not in self._parameters:
                        self._parameters['registration_channel'] = d_string("Registration Channel", "", "Which channel to use to compute image transformations")
                    registration_channel = self['registration_channel']
                    self._parameters['registration_channel'].setValues(list(img.keys()))
                    if registration_channel in img.keys():
                        self._parameters['registration_channel'].setValue(registration_channel)
                    else:
                        self._parameters['registration_channel'].setValue(list(img.keys())[0])

    def run(self):
        self.out_df = {}
        self.trajectory_df = {}
        
        for time in self.df.keys():
            df = self.df[time]
            # #}
            # implement the run method

            self.out_df[time] = deepcopy(df)

        time_points = np.sort([t for t in self.df])

        sequence_nuclei_data = {}
        sequence_nuclei_points = {}
        for time in time_points:
            nuclei_df = self.out_df[time]
            sequence_nuclei_data[time] = nuclei_df
            sequence_nuclei_points[time] = dict(zip(
                nuclei_df['label'].values,
                nuclei_df[[f'center_{dim}' for dim in 'xyz']].values
            ))

        sequence_rigid_transforms = {}
        sequence_affine_transforms = {}
        sequence_vectorfield_transforms = {}
        for previous_time, next_time in zip(time_points[:-1], time_points[1:]):
            previous_img = self.img[previous_time].get_channel(self["registration_channel"])
            next_img = self.img[next_time].get_channel(self["registration_channel"])
        
            rigid_trsf = blockmatching(
                reference_image=previous_img,
                floating_image=next_img,
                method='rigid',
                pyramid_highest_level=self["registration_pyramid_highest"],
                pyramid_lowest_level=self["registration_pyramid_lowest"]
            )
            sequence_rigid_transforms[(previous_time, next_time)] = rigid_trsf
        
            affine_trsf = blockmatching(
                reference_image=previous_img,
                floating_image=next_img,
                init_trsf=rigid_trsf,
                method='affine',
                pyramid_highest_level=self["registration_pyramid_highest"],
                pyramid_lowest_level=self["registration_pyramid_lowest"]
            )
            sequence_affine_transforms[(previous_time, next_time)] = affine_trsf
        
            vectorfield_trsf = blockmatching(
                reference_image=previous_img,
                floating_image=next_img,
                init_trsf=affine_trsf,
                method='vectorfield',
                pyramid_highest_level=self["registration_pyramid_highest"]-2,
                pyramid_lowest_level=self["registration_pyramid_lowest"]-1
            )
            sequence_vectorfield_transforms[(previous_time, next_time)] = vectorfield_trsf

        gap_range = range(1, len(time_points))
        for gap in gap_range[1:]:
            for previous_time, time, next_time in zip(time_points[:-gap], time_points[1:-(gap-1)], time_points[gap:]):
                # print((time, next_time))

                previous_transform = sequence_rigid_transforms[(previous_time, time)]
                next_transform = sequence_rigid_transforms[(time, next_time)]
                #sequence_rigid_transforms[(previous_time, next_time)] = compose_trsf([previous_transform, next_transform])
                sequence_rigid_transforms[(previous_time, next_time)] = compose_trsf([next_transform, previous_transform])
        
                previous_transform = sequence_vectorfield_transforms[(previous_time, time)]
                next_transform = sequence_vectorfield_transforms[(time, next_time)]
                #sequence_vectorfield_transforms[(previous_time, next_time)] = compose_trsf([previous_transform, next_transform])
                sequence_vectorfield_transforms[(previous_time, next_time)] = compose_trsf([next_transform, previous_transform])

        sequence_registered_points = {}
        sequence_registered_points[time_points[0]] = sequence_nuclei_points[time_points[0]]
        for time in time_points[1:]:
            if (time_points[0], time) in sequence_rigid_transforms:
                nuclei_points = sequence_nuclei_points[time]
                rigid_trsf = inv_trsf(sequence_rigid_transforms[(time_points[0], time)])

                registered_points = dict(zip(
                    [n for n in nuclei_points],
                    apply_trsf_to_points([nuclei_points[n] for n in nuclei_points], rigid_trsf)
                ))
                sequence_registered_points[time] = registered_points

        for time in time_points:
            for k, dim in enumerate('xyz'):
                sequence_nuclei_data[time][f'registered_{dim}'] = [
                    sequence_registered_points[time][n][k]
                    for n in sequence_nuclei_data[time]['label'].values
                ]

        G = nx.DiGraph()
        nuclei_nodes = [(t, n) for t in time_points for n in sequence_registered_points[t]]
        nuclei_times = {(t, n): t for t, n in nuclei_nodes}
        nuclei_labels = {(t, n): n for t, n in nuclei_nodes}

        G.add_nodes_from(nuclei_nodes)
        nx.set_node_attributes(G, nuclei_times, 'hour_time')
        nx.set_node_attributes(G, nuclei_labels, 'label')

        signal_names = [c for c in sequence_nuclei_data[time_points[0]].columns if c not in ['label', 'hour_time']]
        signal_names = [s for s in signal_names if s in sequence_nuclei_data[time_points[0]].columns]
        print(signal_names)
        for column in signal_names:
            nuclei_signal_dict = {
                (t, l): s
                for t in time_points
                for l, s in sequence_nuclei_data[t][['label', column]].values
            }
            nuclei_signal = {
                (t, n): nuclei_signal_dict[(t, n)] for t, n in nuclei_nodes
            }
            nx.set_node_attributes(G, nuclei_signal, column)

        for gap in gap_range:
            for previous_time, next_time in zip(time_points[:-gap], time_points[gap:]):
                if (previous_time, next_time) in sequence_vectorfield_transforms:
                    # print((time, next_time))
                    previous_points = sequence_nuclei_points[previous_time]
                    next_points = sequence_nuclei_points[next_time]
                    vectorfield_trsf = sequence_vectorfield_transforms[(previous_time, next_time)]
                    #vectorfield_trsf = sequence_nuclei_vectorfield_transforms[(previous_time, next_time)]
        
                    registered_points = dict(zip(
                        [n for n in previous_points],
                        apply_trsf_to_points([previous_points[n] for n in previous_points], vectorfield_trsf)
                    ))
        
                    point_distances = cdist(
                        [next_points[n] for n in next_points],
                        [registered_points[n] for n in previous_points]
                    )
                    point_distances[point_distances > self['distance_threshold']] = np.nan
                    lineage_dict = dict(zip(
                        [n for n in next_points],
                        [[n for n in previous_points][np.nanargmin(d)] if not np.all(np.isnan(d)) else 0 for d in point_distances]
                    ))
        
                    lineage_edges = [
                        ((previous_time, p), (next_time, n))
                        for n, p in lineage_dict.items()
                        if p != 0
                           and G.in_degree[(next_time, n)]==0
                    ]
                    # print(len(lineage_edges),"/",len(lineage_dict))
                    G.add_edges_from(lineage_edges)

            source_nodes = [n for n, d in G.in_degree if d==0]
            sink_nodes = [n for n, d in G.out_degree if d==0]
    
            unmapped_nodes = [(t, n) for t, n in source_nodes if t != time_points[0]]
            non_first_nodes = [(t, n) for t, n in G.nodes if t != time_points[0]]
    
            print(f"{np.round(100*len(unmapped_nodes) / len(non_first_nodes), 2)}% unmapped nuclei")

        first_nodes = [(t, n) for t, n in G.nodes if t == time_points[0]]
        last_nodes = [(t, n) for t, n in G.nodes if t == time_points[-1]]
        complete_trajectories = [
            list(nx.all_simple_paths(G, source=f, target=l))
            for f in first_nodes for l in last_nodes
            if nx.has_path(G, source=f, target=l)
        ]
        print(f"{np.round(100*len(complete_trajectories) / len(last_nodes), 2)}% complete trajectories")

        if len(time_points)>2:
            next_nodes = [(t, n) for t, n in source_nodes if t == time_points[1]]
            previous_nodes = [(t, n) for t, n in sink_nodes if t == time_points[-2]]

            missing_end_trajectories = []
            missing_end_trajectories += [
                list(nx.all_simple_paths(G, source=f, target=l))
                for f in first_nodes for l in previous_nodes
                if nx.has_path(G, source=f, target=l)
            ]
            missing_end_trajectories += [
                list(nx.all_simple_paths(G, source=f, target=l))
                for f in next_nodes for l in last_nodes
                if nx.has_path(G, source=f, target=l)
            ]
            if len(time_points)>3:
                missing_end_trajectories += [
                    list(nx.all_simple_paths(G, source=f, target=l))
                    for f in next_nodes for l in previous_nodes
                    if nx.has_path(G, source=f, target=l)
                ]
            print(f"{np.round(100*len(missing_end_trajectories) / len(last_nodes), 2)}% missing-end trajectories")
            if self['include_incomplete']:
                complete_trajectories += missing_end_trajectories

        sequence_ancestors = {}
        sequence_ancestor_times = {}
        for time in time_points:
            nuclei_df = sequence_nuclei_data[time]
            sequence_ancestors[time] = {n: pd.NA if time>time_points[0] else n for n in nuclei_df['label'].values}
            sequence_ancestor_times[time] = {n: pd.NA if time>time_points[0] else time for n in nuclei_df['label'].values}

        for ((a_t, a_l), (t, l)) in list(G.edges):
            sequence_ancestors[t][l] = a_l
            sequence_ancestor_times[t][l] = a_t

        trajectory_data = {
            col: [] for col in ["trajectory_label", "label", "time"]
        }
        for i_t, traj in enumerate(complete_trajectories):
            for (t, n) in traj[0]:
                trajectory_data["trajectory_label"] += [i_t]
                trajectory_data["label"] += [n]
                trajectory_data["time"] += [t]

        for time in time_points:
            nuclei_df = sequence_nuclei_data[time]
            nuclei_df['ancestor'] = [sequence_ancestors[time][n] for n in nuclei_df['label'].values]
            nuclei_df['ancestor_time'] = [sequence_ancestor_times[time][n] for n in nuclei_df['label'].values]

        trajectory_df = pd.DataFrame(trajectory_data)
        for c in signal_names:
            trajectory_df[c] = [G.nodes[(t, n)][c] for t, n in trajectory_df[['time', 'label']].values]
        
        self.trajectory_df[0] = trajectory_df
