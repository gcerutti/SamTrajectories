# {# gnomon, plugin.imports
from dtkcore import d_int, d_real, d_inliststring

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, imageOutput, binaryImageOutput
# #}
# add your imports before the next gnomon tag

from copy import deepcopy

import numpy as np
import scipy.ndimage as nd

from skimage.filters import threshold_otsu

from timagetk import SpatialImage, MultiChannelImage


# {# gnomon, plugin.class

@algorithmPlugin(version='0.1.1', coreversion='1.0.0', name='SurfCut')
@imageInput(attr='img', data_plugin='gnomonImageDataMultiChannelImage')
@imageOutput(attr='filtered_img', data_plugin='gnomonImageDataMultiChannelImage')
@binaryImageOutput(attr='binary_img', data_plugin='binaryImageDataSpatialImage')
class imageErosionSurfCut(gnomon.core.gnomonAbstractFormAlgorithm):
    """Extract the upper surface of a 3D image using the SurfCut algorithm.

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['channel_name'] = d_inliststring('Channel', '', [''], 'Channel on which to extract the surface')
        self._parameters['orientation'] = d_inliststring('Orientation', "up", ["up", "down"], "Whether to keep upper or lower part")
        self._parameters['gaussian_sigma'] = d_real('Gaussian sigma', 1., 0., 5., 1, 'Sigma for the Gaussian smoothing (in µm)')
        self._parameters['top_erosion'] = d_int('Top erosion', 8, 0, 20, 'Number of erosion steps to reach surface top')
        self._parameters['bottom_erosion'] = d_int('Bottom erosion', 15, 0, 20, 'Number of erosion steps to reach surface bottom')

        self.img = {}
        self.filtered_img = {}
        self.binary_img = {}

    def refreshParameters(self):
        if len(self.img)>0:
            images = list(self.img.values())[0]
            if len(images) == 1:
                if 'channel_name'  in self._parameters.keys():
                    del self._parameters['channel_name']
            else:
                if 'channel_name' not in self._parameters.keys():
                    self._parameters['channel_name'] = d_inliststring('Channel', '', [''], 'Channel on which to extract the surface')
                    channel_name = images.channel_names[0]
                else:
                    channel_name = self['channel_name']
                self._parameters['channel_name'].setValues(images.channel_names)
                self._parameters['channel_name'].setValue(channel_name)

    def run(self):
        self.filtered_img = {}
        for time in self.img.keys():
            img = self.img[time]
            # #}
            # implement the run method

            channel_name = self['channel_name'] if 'channel_name' in self._parameters.keys() else list(img.keys())[0]
            img = img.get_channel(channel_name)

            padded_img = img.get_array()
            pad_height = int(2*np.ceil(self['gaussian_sigma']/img.voxelsize[0]))
            if self['orientation'] == "up":
                padded_img = np.concatenate([padded_img, np.zeros_like(padded_img)[:pad_height]], axis=0)
            elif self['orientation'] == "down":
                padded_img = np.concatenate([np.zeros_like(padded_img)[:pad_height], padded_img], axis=0)

            smooth_img = nd.gaussian_filter(padded_img, sigma=self['gaussian_sigma']/np.array(img.voxelsize))

            threshold = threshold_otsu(smooth_img.ravel())
            binary_img = smooth_img>threshold
            binary_img = np.array([nd.binary_fill_holes(i) for i in binary_img])

            zzz, _, _ = np.mgrid[0:binary_img.shape[0], 0:binary_img.shape[1], 0:binary_img.shape[2]].astype(float)
            proj = zzz * (binary_img != 0)
            proj[proj == 0] = np.nan
            if self['orientation'] == "up":
                proj = np.nanmax(proj, axis=0)
                proj[np.isnan(proj)] = 0
                binary_img = zzz <= proj[np.newaxis]
                self.binary_img[time] = SpatialImage(binary_img[:-pad_height], voxelsize=img.voxelsize)
            elif self['orientation'] == "down":
                proj = np.nanmin(proj, axis=0)
                proj[np.isnan(proj)] = binary_img.shape[0] - 1
                binary_img = zzz >= proj[np.newaxis]
                self.binary_img[time] = SpatialImage(binary_img[pad_height:], voxelsize=img.voxelsize)

            top_img = nd.binary_erosion(binary_img, iterations=self['top_erosion'], border_value=1)
            bottom_img = nd.binary_erosion(binary_img, iterations=self['bottom_erosion'], border_value=1)

            mask_img = np.logical_xor(top_img, bottom_img)

            if self['orientation'] == "up":
                mask_img = mask_img[:-pad_height]
            else:
                mask_img = mask_img[pad_height:]

            filtered_img = {}
            for c in self.img[time].keys():
                surf_img = deepcopy(self.img[time].get_channel(c))
                surf_img[mask_img==0] = 0
                filtered_img[c] = surf_img

            self.filtered_img[time] = MultiChannelImage(filtered_img)
