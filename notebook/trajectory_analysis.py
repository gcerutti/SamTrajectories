import logging
from typing import Optional, List

from tqdm import tqdm

import numpy as np
import pandas as pd

from scipy.interpolate import splrep, splev
from scipy.integrate import simps


# Post-processing of trajectories: Filtering & Interpolation

def filter_trajectories(
    input_trajectory_df: pd.DataFrame, l1_only=True,
    dome_threshold: Optional[float]=0.2,
    r_max: Optional[int]=65, length_min: Optional[int]=6
) -> pd.DataFrame:
    """Filter out trajectories depending on several parameters.

    Parameters
    ----------
    input_trajectory_df
        The DataFrame containing trajectories to filter
    l1_only
        Whether to exclude trajectories which do not belong to the L1 layer
    dome_threshold
        The percentage of nuclei outside the dome in a trajectory above which it should be excluded
    r_max
        The threshold for mean radial distance (in micrometers): trajectories with a mean radial distance above this threshold are filtered out
    length_min
        The threshold for trajectory length (in number of timepoints): trajectories shorter than this length threshold are filtered out

    Returns
    -------
    trajectory_df : pd.DataFrame
        The DataFrame with filtered trajectories

    """
    trajectory_df = input_trajectory_df.copy()
    hour_times = np.unique(trajectory_df['hour_time'])

    if l1_only:
        if 'layer' in trajectory_df.columns:
            trajectory_layer = trajectory_df.groupby('trajectory_label').max()['layer'].to_dict()
            trajectory_df['trajectory_layer'] = [trajectory_layer[l] for l in trajectory_df['trajectory_label'].values]
            trajectory_df = trajectory_df.query("trajectory_layer==1").copy()
        else:
            logging.warning("No column 'layer' in trajectory DataFrame! Can not filter l1_only")

    if dome_threshold is not None:
        if 'organ' in trajectory_df.columns:
            trajectory_dome = trajectory_df.groupby('trajectory_label').mean()['organ'].to_dict()
            trajectory_df['organ'] = [trajectory_dome[l] for l in trajectory_df['trajectory_label'].values]
            trajectory_df = trajectory_df.query(f"organ<={dome_threshold}").copy()
        else:
            logging.warning("No column 'organ' in trajectory DataFrame! Can not filter on dome_threshold")

    if r_max is not None:
        if 'radial_distance' in trajectory_df.columns:
            trajectory_radial_distances = trajectory_df.groupby('trajectory_label').mean()['radial_distance'].to_dict()
            trajectory_df['trajectory_radial_distance'] = [trajectory_radial_distances[l] for l in trajectory_df['trajectory_label'].values]
            trajectory_df = trajectory_df.query(f"trajectory_radial_distance<{r_max}").copy()
        else:
            logging.warning("No column 'radial_distance' in trajectory DataFrame! Can not filter on r_max")

    if length_min is not None:
        trajectory_lengths = trajectory_df.groupby('trajectory_label').count()['label'].to_dict()
        trajectory_df['trajectory_length'] = [trajectory_lengths[l] for l in trajectory_df['trajectory_label'].values]
        trajectory_df = trajectory_df.query(f"trajectory_length>={length_min}").copy()

    return trajectory_df
    

def interpolate_trajectories(
    trajectory_df: pd.DataFrame, extrapolate: bool=True
) -> pd.DataFrame:
    """Interpolate linearly trajectory data in missing time points.

    Parameters
    ----------
    trajectory_df
        The DataFrame containing trajectories to interpolate
    extrapolate
        Whether to include trajectories where extrapolation is needed

    Returns
    -------
    interpolated_trajectory_df : pd.DataFrame
        The DataFrame with interpolated trajectories

    """
    trajectory_labels = np.unique(trajectory_df['trajectory_label'])
    hour_times = np.unique(trajectory_df['hour_time'])
    interpolated_trajectory_data = []
    for l in tqdm(trajectory_labels, unit='trajectory', desc=f"Interpolating {len(trajectory_labels)} trajectories"):
        label_df = trajectory_df.query(f"trajectory_label=={l}").copy()
        label_df['interpolated'] = False
        if len(label_df) != len(hour_times):
            label_times = sorted(label_df['hour_time'].values)
            missing_times = [t for t in hour_times if t not in label_times]
            if extrapolate or all([label_times[0] < t < label_times[-1] for t in missing_times]):
                interpolated_rows = []
                for t in missing_times:
                    if t < label_times[0]:
                        t_min = label_times[0]
                        t_max = label_times[1]
                    elif t > label_times[-1]:
                        t_min = label_times[-2]
                        t_max = label_times[-1]
                    else:
                        t_min = [i_t for i_t in label_times if i_t < t][-1]
                        t_max = [i_t for i_t in label_times if i_t > t][0]
                    t_min_w = (t_max - t)/(t_max - t_min)
                    t_max_w = (t - t_min)/(t_max - t_min)
                    if t_min == t_max:
                        logging.debug(f"Extrapolating t={t}h as {t_min_w}*t{t_min}h + {t_max_w}*t{t_max}h {label_times}")

                    interpolated_rows += [pd.concat([
                        label_df.query(f"hour_time=={t_min}")*t_min_w,
                        label_df.query(f"hour_time=={t_max}")*t_max_w
                    ]).agg(['sum'])]
                for row_df in interpolated_rows:
                    row_df['interpolated'] = True
                interpolated_df = pd.concat([label_df] + interpolated_rows, ignore_index=True).sort_values('hour_time')
                interpolated_trajectory_data += [interpolated_df]
        else:
            interpolated_trajectory_data += [label_df]
    interpolated_trajectory_df = pd.concat(interpolated_trajectory_data)
    for column in ['time', 'hour_time', 'label', 'trajectory_label']:
        interpolated_trajectory_df[column] = [int(np.round(v)) for v in interpolated_trajectory_df[column].values]

    return interpolated_trajectory_df


def interpolate_spline_trajectories(trajectory_df, degree=1, time_step=None, smooth=False):
    """Interpolate non-linearly trajectory data in missing time points.

    Parameters
    ----------
    trajectory_df
        The DataFrame containing trajectories to interpolate
    degree
        The polynomial degree of the spline
    time_step
        The time step value for interpolation (by default, time step value corresponds to the timepoints)
    smooth
        Whether to smoothen the curve resulting from the interpolation

    Returns
    -------
    interpolated_trajectory_df : pd.DataFrame
        The DataFrame with interpolated trajectories

    """

    trajectory_labels = np.unique(trajectory_df['trajectory_label'])
    hour_times = np.unique(trajectory_df['hour_time'])
    if time_step is None:
        time_range = hour_times
    else:
        n_steps = int(np.round((hour_times.max() - hour_times.min())/time_step))
        time_range = np.linspace(hour_times.min(), hour_times.max(), n_steps+1)

    interpolated_trajectory_data = []
    for l in tqdm(trajectory_labels, unit='trajectory', desc=f"Interpolating {len(trajectory_labels)} trajectories"):
    
        t_df = trajectory_df.query(f"trajectory_label=={l}")
        
        spline_t_df = pd.DataFrame()
        spline_t_df['hour_time'] = time_range
        
        for col in t_df.columns:
            if col not in ['hour_time', 'label']:
                bs = splrep(
                    t_df['hour_time'].values, t_df[col].values, k=degree, quiet=True
                )
                spline_t_df[col] = splev(spline_t_df['hour_time'].values, bs)
        interpolated_trajectory_data += [spline_t_df]

    interpolated_trajectory_df = pd.concat(interpolated_trajectory_data)
    for column in ['trajectory_label']:
        interpolated_trajectory_df[column] = [int(np.round(v)) for v in interpolated_trajectory_df[column].values]
        
    return interpolated_trajectory_df


# Differential calculus on trajectories: Derivation & Integration

import pandas as pd


def compute_previous_signal_values(trajectory_df: pd.DataFrame, columns: list[str]) -> None:
    """
    Adds new columns with previous time point values for the given columns.

    Parameters
    ----------
    trajectory_df: pd.DataFrame
        DataFrame containing trajectory data with 'trajectory_label' and 'hour_time' columns.
    columns: list[str]
        List of column names to compute previous values for.

    Returns
    -------
    None

    Notes
    -----
    Modifies trajectory_df in place with new columns 'previous_<column>' for each specified column.
    """
    missing_columns = [col for col in columns if col not in trajectory_df.columns]
    if missing_columns:
        raise ValueError(f"Columns {missing_columns} do not exist in trajectory_df.")

    trajectory_labels = trajectory_df['trajectory_label'].unique()

    for l in trajectory_labels:
        label_mask = trajectory_df['trajectory_label'] == l
        label_df = trajectory_df[label_mask]
        sorted_label_df = label_df.sort_values('hour_time')

        for column in columns:
            previous_values = sorted_label_df[column].shift(1)
            trajectory_df.loc[label_mask, f"previous_{column}"] = previous_values


def compute_discrete_temporal_derivatives(trajectory_df: pd.DataFrame, signal_list=['AHP6','Auxin','CLV3']):
    """Compute the discrete temporal derivative of several signals at each time point.

    Parameters
    ----------
    trajectory_df
        The DataFrame containing signal values to derivate
    signal_list
        The list containing the names of the signals to derivate

    Returns
    -------
    None

    Notes
    -----
    Adds columns to the DataFrame, containing computed temporal derivatives (1 column per signal)

    """

    timepoints_list = np.unique(trajectory_df['hour_time'])

    for signal_name in signal_list:
        signal_time_derivatives = {}
        trajectory_labels = trajectory_df['trajectory_label'].unique()
        for l in trajectory_labels:
            label_df = trajectory_df.query(f"trajectory_label == {l}")
            sorted_label_df = label_df.sort_values('hour_time')
            signal_trajectory_list = [s for s in sorted_label_df[f"{signal_name}"]]

            signal_derivatives_trajectory = [(signal_trajectory_list[1]-signal_trajectory_list[0])/(timepoints_list[1]-timepoints_list[0])]

            for t in range(1, len(timepoints_list)-1):
                signal_derivatives_trajectory.append(np.mean([
                    (signal_trajectory_list[t+1] - signal_trajectory_list[t])/(timepoints_list[t+1] - timepoints_list[t]),
                    (signal_trajectory_list[t] - signal_trajectory_list[t-1])/(timepoints_list[t] - timepoints_list[t-1])
                ]))

            signal_derivatives_trajectory.append((signal_trajectory_list[-1] - signal_trajectory_list[-2])/(timepoints_list[-1] - timepoints_list[-2]))

            signal_time_derivatives_trajectory = dict(zip(timepoints_list, signal_derivatives_trajectory))
            signal_time_derivatives[l] = signal_time_derivatives_trajectory

        trajectory_df[f"d{signal_name}/dt"] = [
            signal_time_derivatives[label][time]
            for label, time in zip(trajectory_df['trajectory_label'],trajectory_df['hour_time'])
        ]


def compute_auxin_temporal_integral_simpson_rule(trajectory_df: pd.DataFrame, A_int=0, A0=0):
    """Compute the temporal integral of auxin values between T00 and each timepoint, using Simpson's rule.

    The integral can be truncated (calculation of the area between the auxin curve and a minimal auxin perception threshold A_int),
    or considered null if below a minimal auxin integral value A0.

    Parameters
    ----------
    trajectory_df
        The DataFrame containing the auxin values to integrate
    A_int
        The auxin threshold value for integral truncation
    A0
        The auxin integral threshold value for integral non-nullification

    Returns
    -------
    None

    Notes
    -----
    Adds a column to the DataFrame, containing computed auxin temporal integrals

    """

    timepoints_list = np.unique(trajectory_df['hour_time'])

    auxin_time_integrals = {}
    trajectory_labels = trajectory_df['trajectory_label'].unique()
    for l in trajectory_labels:
        label_df = trajectory_df.query(f"trajectory_label == {l}")
        sorted_label_df = label_df.sort_values('hour_time')
        auxin_trajectory_list = [a - A_int for a in sorted_label_df['Auxin']]

        auxin_integrals_trajectory = []
        for t in range(1, len(timepoints_list)+1):
            if auxin_trajectory_list[t-1] > 0:
                auxin_t_integral = simps(auxin_trajectory_list[0:t:1])
                auxin_integrals_trajectory.append(auxin_t_integral) if auxin_t_integral > A0 else auxin_integrals_trajectory.append(0)
            else:
                auxin_integrals_trajectory.append(0)

        auxin_time_integrals_trajectory = dict(zip(timepoints_list, auxin_integrals_trajectory))
        auxin_time_integrals[l] = auxin_time_integrals_trajectory

    trajectory_df['Int(Auxin)dt'] = [
        auxin_time_integrals[label][time]
        for label, time in zip(trajectory_df['trajectory_label'],trajectory_df['hour_time'])
    ]


def estimate_auxin_temporal_integral_initial_values(
    trajectory_df: pd.DataFrame, dev_time_range, cluster_variable=None
):
    """Estimate the initial value of the auxin integral for different developmental times.

    The function computes the auxin integral over 1 plastochron (2 time intervals) from
    sub-trajectories of primordia cells with the same developmental time. These integral
    values are then averaged and added to the previous one to estimate the value of the
    auxin integral at T=0h depending on the developmental stage of the cell.

    Parameters
    ----------
    trajectory_df
        The DataFrame containing the auxin values to integrate
    dev_time_range
        The values of developmental time for which to estimate an initial value
    cluster_variable
        The column on which to cluster data to make estimates per cluster

    """
    estimated_auxin_initial_integrals = {}
    dev_time_plastochron_integrals = {}

    if cluster_variable is not None and cluster_variable in trajectory_df.columns:
        clusters = np.unique(trajectory_df[cluster_variable].values)
        clusters = [c for c in clusters if not pd.isnull(c)]
    else:
        clusters = [None]

    for cluster in clusters:
        if cluster is not None:
            estimated_auxin_initial_integrals[cluster] = {}
            dev_time_plastochron_integrals[cluster] = {}
            cluster_df = trajectory_df.query(f"{cluster_variable}=={cluster}")
        else:
            cluster_df = trajectory_df
        for dev_time in dev_time_range:
            dev_time_df = cluster_df.query(f"primordium_developmental_time=={dev_time}")
            trajectory_labels = dev_time_df['trajectory_label'].unique()
            auxin_integrals = []
            for l in trajectory_labels:
                label_dev_time_df = trajectory_df.query(f"trajectory_label == {l} & primordium_developmental_time>={dev_time}")
                auxin_trajectory_list = label_dev_time_df.sort_values('hour_time')['Auxin'].values
                if len(auxin_trajectory_list)>=3:
                    auxin_integrals.append(simps(auxin_trajectory_list[0:3]))

            if cluster is not None:
                dev_time_plastochron_integrals[cluster][dev_time+1] = auxin_integrals
                estimated_auxin_initial_integrals[cluster][dev_time+1] = estimated_auxin_initial_integrals[cluster].get(dev_time, 0) + np.mean(auxin_integrals)
            else:
                dev_time_plastochron_integrals[dev_time+1] = auxin_integrals
                estimated_auxin_initial_integrals[dev_time+1] = estimated_auxin_initial_integrals.get(dev_time, 0) + np.mean(auxin_integrals)

    return estimated_auxin_initial_integrals, dev_time_plastochron_integrals


def reconstruct_auxin_temporal_integral(
    trajectory_df: pd.DataFrame, integration_start_time: Optional[float]=None,
    cluster_variable: Optional[str]=None,
):
    """Reconstruct the value of the auxin integral in primordia cells.

    The function estimates the initial value (at T=0h) of the auxin integral
    using the average integral value over the time interval between the
    integration start time and the initial developmental time of the primordium.

    This value is added to the actual auxin integral over the trajectory,
    which is computed if it is not already present in the data.

    Parameters
    ----------
    trajectory_df
        The DataFrame containing the auxin values to integrate
    integration_start_time
        The starting point (developmental time) where the auxin integral is set to 0. By default,
        set to the first represented developmental time.
    cluster_variable
        The column to use to estimate different integral initial values per cluster

    """

    if not 'Int(Auxin)dt' in trajectory_df.columns:
        logging.warning(f"Auxin integral has not been computed; it will be computed using default method.")
        compute_auxin_temporal_integral_simpson_rule(trajectory_df)

    dev_time_range = np.unique(
        trajectory_df.query('hour_time == 0')['primordium_developmental_time'].values
    )
    if integration_start_time is None:
        integration_start_time = np.nanmin(dev_time_range)
    dev_time_range = [t for t in dev_time_range if t>=integration_start_time]

    estimated_auxin_initial_integrals, dev_time_plastochron_integrals = estimate_auxin_temporal_integral_initial_values(
        trajectory_df, dev_time_range, cluster_variable=cluster_variable
    )

    if cluster_variable is not None and cluster_variable in trajectory_df.columns:
        clusters = estimated_auxin_initial_integrals.keys()
        cluster_trajectories_starting_in_dev_time = {
            cluster: {
                dev_time: trajectory_df.query(f"{cluster_variable}=={cluster} & hour_time==0 & primordium_developmental_time=={dev_time}")['trajectory_label'].values
                for dev_time in dev_time_range
            }
            for cluster in clusters
        }
        conditions = [np.array([l in cluster_trajectories_starting_in_dev_time[cluster][dev_time] for l in trajectory_df['trajectory_label']]) for cluster in clusters for dev_time in dev_time_range]
        actions = [(trajectory_df['Int(Auxin)dt'] + estimated_auxin_initial_integrals.get(cluster, {}).get(dev_time, 0)) for cluster in clusters for dev_time in dev_time_range]
    else:
        trajectories_starting_in_dev_time = {
            dev_time: trajectory_df.query(f"hour_time==0 & primordium_developmental_time=={dev_time}")['trajectory_label'].values
            for dev_time in dev_time_range
        }
        conditions = [np.array([l in trajectories_starting_in_dev_time[dev_time] for l in trajectory_df['trajectory_label']]) for dev_time in dev_time_range]
        actions = [(trajectory_df['Int(Auxin)dt'] + estimated_auxin_initial_integrals.get(dev_time, 0)) for dev_time in dev_time_range]

    trajectory_df['Reconst_Int(Auxin)dt'] = np.select(conditions, actions, default=np.nan)

    return estimated_auxin_initial_integrals, dev_time_plastochron_integrals


def compute_signal_delta_at_primordium_developmental_time(
    trajectory_df: pd.DataFrame, signal_name: str, normalize: bool=True
):
    """Compute relative values of a given signal per primordium developmental time

    The function considers all the cells at the same primordium developmental time
    to compute relative values (deltas) by subtracting the mean value of the
    signal at that time, and store them a new signal. These relative values
    may be normalized, in which case they are divided by the mean values.

    Parameters
    ----------
    trajectory_df
        The DataFrame on which to compute the relative values
    signal_name
        The signal for which to compute the relative values
    normalize
        Whether to normalize the relative values

    """
    time_column = 'primordium_developmental_time'
    time_means = trajectory_df.groupby(time_column)[signal_name].mean()

    trajectory_df[f"{'normalized_' if normalize else ''}delta_{signal_name}"] = [
        (value - mean)/mean if normalize else value - mean
        for value, mean in zip(trajectory_df[signal_name].values, trajectory_df[time_column].map(time_means))
    ]
