import os
import importlib
import argparse

import gnomon.core
import gnomon.visualization

from dtkcore import array_real_2

from gnomon.utils.gnomonPlugin import load_plugin_group
from gnomon.utils.views import gnomonStandaloneVtkView, gnomonStandaloneMplView


def initialize_plugins():
    load_plugin_group('imageReader')
    load_plugin_group('imageFilter')
    load_plugin_group('imageVtkVisualization')
    load_plugin_group('meshFromImage')
    load_plugin_group('meshFilter')
    load_plugin_group('meshVtkVisualization')
    load_plugin_group('meshWriter')
    load_plugin_group('pointCloudFromImage')
    load_plugin_group('pointCloudQuantification')
    load_plugin_group('pointCloudVtkVisualization')
    load_plugin_group('pointCloudWriter')
    load_plugin_group('dataFrameMplVisualization')
    load_plugin_group('dataFrameWriter')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--project-path', help='Path to the gnomon project directory', default="./")
    parser.add_argument('-f', '--filename', help='SAM image sequence identifier', required=True)
    parser.add_argument('-t', '--times', default=[0], nargs='+', help='List of individual time points to process', type=int)
    parser.add_argument('-m', '--microscopy-directory', help='Relative path to CZI image directory [default : microscopy/]', default=None)
    parser.add_argument('-r', '--results-directory', help='Relative path to output directory [default : results/]', default=None)
    parser.add_argument('-c', '--channel-names', help='List of chanel names in the same order as in the microscopy image', nargs='+', type=str, required=True)
    parser.add_argument('-o', '--sam-orientation', help='Clockwise or counterclockwise orientation of the SAM', default="clockwise", choices=["clockwise", "counterclockwise"])
    parser.add_argument('-mo', '--microscope-orientation', help='Upright or inverted orientation of the microscope', default="upright", choices=["upright", "inverted"])
    args = parser.parse_args()

    initialize_plugins()

    project_path = args.project_path # "/Users/gcerutti/Projects/RDP/HugoNucleiTrajectories/"
    filename = args.filename # "qDII CLV AHP6 PI SAM6"
    file_times = args.times # [0, 6]
    channel_names = args.channel_names # ["DIIV", "AHP6", "TagBFP", "CLV3", "PI"]
    sam_orientation = args.sam_orientation # "clockwise"
    microscope_orientation = args.microscope_orientation # "upright"

    microscopy_directory = args.microscopy_directory if args.microscopy_directory is not None else "microscopy/" #data/AHP6 exK1
    microscopy_dirname = f"{project_path}/{microscopy_directory}"

    reporter_names = [c for c in channel_names if c not in ['TagBFP', 'PI', 'DIIV', 'CLV3']]

    output_directory = args.results_directory if args.results_directory is not None else "results/"
    output_dirname = f"{project_path}/{output_directory}"
    if not os.path.exists(f"{output_dirname}/{filename}"):
        os.makedirs(f"{output_dirname}/{filename}")

    reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
    paths = [f"{microscopy_dirname}/{filename}-T{str(t).zfill(2)}.czi" for t in file_times]
    path = "".join([f",{p}" for p in paths])[1:]
    reader.setPath(path)
    reader.run()

    old_channel_names = reader.image()[0].channels() # ["TaYFP-T1", "TaCFP-T2", "EBFP2-T3", "mCher-T3", PI-T4"]

    rename = gnomon.core.imageFilter_pluginFactory().create("channelNamesEdit")
    rename.setInput(reader.image())
    rename.refreshParameters()
    for old_channel_name, channel_name in zip(old_channel_names, channel_names):
        rename.setParameter(old_channel_name, channel_name)
    rename.run()

    if not os.path.exists(f"{output_dirname}/{filename}/image"):
        os.makedirs(f"{output_dirname}/{filename}/image")
    channel_colormaps = {'TagBFP': '0RGB_blue', 'PI': 'gray', 'CLV3': '0CMY_magenta', 'DIIV': '0CMY_yellow'}
    channel_colormaps.update({c: '0CMY_cyan' for c in reporter_names})

    view = gnomonStandaloneVtkView()
    for channel in channel_names:
        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(rename.output())
        visu.refreshParameters()
        visu.setParameter("channel", channel)
        visu.setParameter("colormap", channel_colormaps[channel])
        visu.setParameter("show_histogram", False)
        visu.setParameter("opacity", 0.2)
        visu.setParameter("contrast", 0.5)
        visu.update()
        visu.setVisible(True)

    for i_t, t in enumerate(file_times):
        view.setCurrentTime(i_t)
        view.updateBounds()
        view.setCameraXY(False, False)
        view.saveScreenshot(f"{output_dirname}/{filename}/image/{filename}-T{str(t).zfill(2)}_image.png")

        view.setCameraXZ(False, False)
        view.saveScreenshot(f"{output_dirname}/{filename}/image/{filename}-T{str(t).zfill(2)}_image_XZ.png")

        view.setCameraYZ(False, False)
        view.saveScreenshot(f"{output_dirname}/{filename}/image/{filename}-T{str(t).zfill(2)}_image_YZ.png")

    plugin_name = "mergeImageChannels"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    merge = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    merge.setInputImage(rename.output())
    merge.refreshParameters()
    merge.setParameter("channels", ['TagBFP', 'CLV3'] + reporter_names)
    merge.setParameter("result_channel", "MERGE")
    merge.run()

    detection = gnomon.core.pointCloudFromImage_pluginFactory().create("nucleiDetectionTimagetk")
    detection.setInput(merge.outputImage())
    detection.refreshParameters()
    detection.setParameter('nuclei_channel', "MERGE")
    # detection.setParameter('radius_range', array_real_2([0.7, 1.4]))
    # detection.setParameter('radius_range', array_real_2([0.5, 1.4]))
    detection.setParameter('radius_range', array_real_2([0.5, 1.2]))
    # detection.setParameter('threshold', 3000)
    detection.setParameter('threshold', 1000)
    # detection.setParameter('threshold', 100)
    detection.run()

    signal = gnomon.core.pointCloudQuantification_pluginFactory().create("nucleiSignalQuantificationTimagetk")
    signal.setPointCloud(detection.output())
    signal.setImage(rename.output())
    signal.refreshParameters()
    signal.setParameter("gaussian_sigma", 0.5)
    signal.run()

    plugin_name = "cellPropertyOperation"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    operation = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    operation.setInputPointCloud(signal.outputPointCloud())
    operation.refreshParameters()
    operation.setParameter("operation", "ratio")
    operation.setParameter("property_a", "DIIV")
    operation.setParameter("property_b", "TagBFP")
    operation.setParameter("result_name", "qDII")
    operation.run()

    meshing = gnomon.core.meshFromImage_pluginFactory().create("imageSurfaceMesh")
    meshing.setImage(merge.outputImage())
    meshing.refreshParameters()
    meshing.setParameter("channel", "MERGE")
    meshing.setParameter("threshold", 1000)
    meshing.setParameter("orientation", "down" if microscope_orientation=='inverted' else 'up')
    meshing.setParameter("resampling_voxelsize", 0.66)
    meshing.setParameter("edge_length", 4.0)
    meshing.run()

    curvature = gnomon.core.meshFilter_pluginFactory().create("triangleMeshCurvature")
    curvature.setInput(meshing.output())
    curvature.refreshParameters()
    curvature.run()

    if not os.path.exists(f"{output_dirname}/{filename}/surface_topomesh"):
        os.makedirs(f"{output_dirname}/{filename}/surface_topomesh")
    writer = gnomon.core.meshWriter_pluginFactory().create("gnomonMeshWriterPropertyTopomesh")
    mesh = curvature.output()
    for i_t, (t, (g_t, m)) in enumerate(zip(file_times, mesh.items())):
        writer.setMesh({t: m})
        writer.setPath(f"{output_dirname}/{filename}/surface_topomesh/{filename}-T{str(t).zfill(2)}_surface_topomesh.ply")
        writer.run()

    colormaps = {'mean_curvature': 'temperature'}
    value_ranges = {'mean_curvature': (-0.05, 0.05)}
    for property_name in ['mean_curvature']:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.meshVtkVisualization_pluginFactory().create("meshVisualizationTopomesh")
        visu.setView(view)
        visu.setMesh(curvature.output())
        visu.refreshParameters()
        visu.setParameter("tube_edges", True)
        visu.setParameter("edge_color", "lightgrey")
        visu.setParameter("attribute", property_name)
        visu.setParameter("value_range", array_real_2(value_ranges[property_name]))
        visu.setParameter("colormap", colormaps[property_name])
        visu.update()

        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(rename.output())
        visu.refreshParameters()
        visu.setParameter("channel", 'PI')
        visu.setParameter("colormap", channel_colormaps['PI'])
        visu.setParameter("show_histogram", False)
        visu.setParameter("opacity", 0.)
        visu.update()

        for i_t, t in enumerate(file_times):
            view.setCurrentTime(i_t)
            view.updateBounds()
            view.setCameraXY(False, False)
            view.saveScreenshot(f"{output_dirname}/{filename}/surface_topomesh/{filename}-T{str(t).zfill(2)}_surface_mesh_{property_name}.png")

    layer = gnomon.core.pointCloudQuantification_pluginFactory().create("nucleiLayerEstimation")
    layer.setPointCloud(operation.outputPointCloud())
    layer.setMesh(curvature.output())
    layer.refreshParameters()
    layer.run()

    property = gnomon.core.pointCloudQuantification_pluginFactory().create("surfaceMeshProperty")
    property.setPointCloud(layer.outputPointCloud())
    property.setMesh(curvature.output())
    property.refreshParameters()
    property.setParameter("attribute_names", ['mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max'])
    property.run()

    if not os.path.exists(f"{output_dirname}/{filename}/nuclei_data"):
        os.makedirs(f"{output_dirname}/{filename}/nuclei_data")
    writer = gnomon.core.pointCloudWriter_pluginFactory().create("pointCloudWriterDataFrame")
    point_cloud = property.outputPointCloud()
    for i_t, (t, (g_t, p_c)) in enumerate(zip(file_times, point_cloud.items())):
        writer.setPointCloud({t: p_c})
        writer.setPath(f"{output_dirname}/{filename}/nuclei_data/{filename}-T{str(t).zfill(2)}_nuclei_data.csv")
        writer.run()

    if not os.path.exists(f"{output_dirname}/{filename}/L1_maps"):
        os.makedirs(f"{output_dirname}/{filename}/L1_maps")
    colormaps = {'qDII': '1Flashy_green', 'CLV3': '1Flashy_purple', 'mean_curvature': 'temperature'}
    colormaps.update({c: '1Flashy_turquoise' for c in reporter_names})
    value_ranges = {'qDII': (0, 0.5), 'CLV3': (0, 15000), 'mean_curvature': (-0.05, 0.05)}
    value_ranges.update({c: (0, 15000) for c in reporter_names})

    for column in ['qDII', 'CLV3', 'mean_curvature']+reporter_names:
        view = gnomonStandaloneMplView()

        visu = gnomon.visualization.dataFrameMplVisualization_pluginFactory().create("matplotlibMapPlot")
        visu.setView(view)
        visu.setDataFrame(property.outputDataFrame())
        visu.refreshParameters()
        visu.setParameter("position_variable", "center")
        visu.setParameter("map_variable", column)
        visu.setParameter("filter_variable", "layer")
        visu.setParameter("filter_range", array_real_2([1, 1]))
        visu.setParameter("radius", 7.5)
        visu.setParameter("density_k", 0.55)
        visu.setParameter("colormap", colormaps[column])
        visu.update()
        visu.setParameter("value_range", array_real_2(list(value_ranges[column])))
        visu.setParameter("contour_range", array_real_2(list(value_ranges[column])))
        visu.update()

        for i_t, t in enumerate(file_times):
            view.setCurrentTime(i_t)
            view._figure.gca().set_xlim(0, 220)
            view._figure.gca().set_ylim(0, 220)
            view.saveScreenshot(f"{output_dirname}/{filename}/L1_maps/{filename}-T{str(t).zfill(2)}_L1_{column}_map.png")

    registration = gnomon.core.pointCloudQuantification_pluginFactory().create("nucleiRigidRegistration")
    registration.setPointCloud(property.outputPointCloud())
    registration.setImage(rename.output())
    registration.refreshParameters()
    registration.setParameter("registration_channel", "TagBFP")
    registration.run()

    alignment = gnomon.core.pointCloudQuantification_pluginFactory().create("clv3qDIISamAlignment")
    alignment.setPointCloud(registration.outputPointCloud())
    alignment.refreshParameters()
    alignment.setParameter("sam_orientation", sam_orientation)
    alignment.setParameter("microscope_orientation", microscope_orientation)
    alignment.setParameter("alignment_times", ["0", "1", "2"])
    alignment.run()

    gnomon.core.formAlgorithm_pluginFactory().clear()
    plugin_name = "nucleiOuterOrganSegmentation"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    outer_organs = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    outer_organs.setInputPointCloud(alignment.outputPointCloud())
    outer_organs.setInputMesh(curvature.output())
    outer_organs.refreshParameters()
    outer_organs.setParameter("gaussian_sigma", 7.5)
    outer_organs.setParameter("merge_threshold", 0.02)
    outer_organs.setParameter("organ_distance", 70.)
    outer_organs.run()

    if not os.path.exists(f"{output_dirname}/{filename}/aligned_nuclei_data"):
        os.makedirs(f"{output_dirname}/{filename}/aligned_nuclei_data")
    colormaps.update({'organ': 'jet'})
    value_ranges.update({'organ': (-0.5, 1.5)})

    view = gnomonStandaloneVtkView()

    visu = gnomon.visualization.pointCloudVtkVisualization_pluginFactory().create("pointCloudVtkVisualization")
    visu.setView(view)
    visu.setPointCloud(outer_organs.outputPointCloud())
    visu.refreshParameters()
    visu.setParameter("scale_factor", 3.)
    visu.setParameter("position", "aligned")
    visu.setParameter("property", "organ")
    visu.setParameter("value_range", array_real_2(list(value_ranges['organ'])))
    visu.setParameter("colormap", colormaps['organ'])
    visu.update()

    for i_t, t in enumerate(file_times):
        view.setCurrentTime(i_t)
        view.updateBounds()
        view.setAxesVisible(True)
        view.setCameraXY(False, False)
        view.saveScreenshot(f"{output_dirname}/{filename}/aligned_nuclei_data/{filename}-T{t:02d}_aligned_L1_nuclei_outer_organs.png")

    plugin_name = "nucleiTrajectories"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    gnomon.core.formAlgorithm_pluginFactory().clear()
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    trajectories = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    trajectories.setInputPointCloud(outer_organs.outputPointCloud())
    trajectories.setInputImage(rename.output())
    trajectories.refreshParameters()
    trajectories.setParameter("registration_channel", "TagBFP")
    trajectories.setParameter("registration_pyramid_lowest", 2)
    trajectories.setParameter("registration_pyramid_highest", 5)
    trajectories.setParameter("distance_threshold", 3.75)
    trajectories.run()

    writer = gnomon.core.pointCloudWriter_pluginFactory().create("pointCloudWriterDataFrame")
    point_cloud = trajectories.outputPointCloud()
    for i_t, (t, (g_t, p_c)) in enumerate(zip(file_times, point_cloud.items())):
        writer.setPointCloud({t: p_c})
        writer.setPath(f"{output_dirname}/{filename}/aligned_nuclei_data/{filename}-T{str(t).zfill(2)}_aligned_nuclei_data.csv")
        writer.run()

    writer = gnomon.core.dataFrameWriter_pluginFactory().create("gnomonDataFrameWriterPandas")
    writer.setDataFrame(trajectories.outputDataFrame())
    writer.setPath(f"{output_dirname}/{filename}/aligned_nuclei_data/{filename}_aligned_nuclei_trajectory_data.csv")
    writer.run()

    for reporter_name in reporter_names:
        gnomon.core.formAlgorithm_pluginFactory().clear()
        plugin_name = "primordiaSignalExtremaDetection"
        plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
        importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

        maxima_detection = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
        maxima_detection.setInputPointCloud(trajectories.outputPointCloud())
        maxima_detection.refreshParameters()
        maxima_detection.setParameter("signal_name", reporter_name)
        maxima_detection.setParameter("extremum_types", ['maximum'])
        maxima_detection.run()

        writer = gnomon.core.dataFrameWriter_pluginFactory().create("gnomonDataFrameWriterPandas")
        maxima_df = maxima_detection.outputDataFrame()
        for i_t, (t, (g_t, df)) in enumerate(zip(file_times, maxima_df.items())):
            writer.setDataFrame({t: df})
            writer.setPath(f"{output_dirname}/{filename}/aligned_nuclei_data/{filename}-T{str(t).zfill(2)}_aligned_{reporter_name}_maxima_data.csv")
            writer.run()

    primordia_detection = gnomon.core.pointCloudQuantification_pluginFactory().create("spiralPhyllotaxisPrimordiaDetection")
    primordia_detection.setPointCloud(trajectories.outputPointCloud())
    primordia_detection.refreshParameters()
    primordia_detection.run()

    writer = gnomon.core.pointCloudWriter_pluginFactory().create("pointCloudWriterDataFrame")
    point_cloud = primordia_detection.outputPointCloud()
    for i_t, (t, (g_t, p_c)) in enumerate(zip(file_times, point_cloud.items())):
        writer.setPointCloud({t: p_c})
        writer.setPath(f"{output_dirname}/{filename}/aligned_nuclei_data/{filename}-T{str(t).zfill(2)}_aligned_primordia_data.csv")
        writer.run()

    if not os.path.exists(f"{output_dirname}/{filename}/aligned_L1_maps"):
        os.makedirs(f"{output_dirname}/{filename}/aligned_L1_maps")

    for column in ['qDII', 'CLV3', 'mean_curvature', 'organ'] + reporter_names:
        view = gnomonStandaloneMplView()

        visu = gnomon.visualization.dataFrameMplVisualization_pluginFactory().create("matplotlibMapPlot")
        visu.setView(view)
        visu.setDataFrame(outer_organs.outputDataFrame())
        visu.refreshParameters()
        visu.setParameter("position_variable", "aligned")
        visu.setParameter("map_variable", column)
        visu.setParameter("filter_variable", "layer")
        visu.setParameter("radius", 7.5)
        visu.setParameter("density_k", 0.55)
        visu.setParameter("colormap", colormaps[column])
        visu.update()
        visu.setParameter("value_range", array_real_2(list(value_ranges[column])))
        visu.setParameter("contour_range", array_real_2(list(value_ranges[column])))
        visu.update()

        for i_t, t in enumerate(file_times):
            view.setCurrentTime(i_t)
            view._figure.gca().set_xlim(-100, 100)
            view._figure.gca().set_ylim(-100, 100)
            view.saveScreenshot(f"{output_dirname}/{filename}/aligned_L1_maps/{filename}-T{str(t).zfill(2)}_aligned_L1_{column}_map.png")
