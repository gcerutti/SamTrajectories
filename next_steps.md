### Next steps

### Data curation
* [x] L1 slices with trajectory nuclei / non-trajectory/discarded nuclei / interpolated nuclei
* [x] ~Towards a (revived) L1 nuclei editor / trajectory editor~ Use TrackMate instead!
* [x] Generate 2D (multichannel) images for manual tracking (REP2 SAM1)
    * [x] Using SurfCut
    * [ ] Using L1-slices
    * [x] Keep the altitude map image 
* [x] Extrapolation of T0 - T36 nuclei?
	* [x] Keep incomplete (missing T0 and/or missing T36) trajectories in main script 
    * [ ] Check on L1 slices + check where

#### Preprocessing
* [x] Organ segmentation (for nuclei removal)
	* [ ] Try using angle + component area
	* [x] Add `organ_index` and `organ_distance` 
	* [x] Gather with AHP6 max script into main script
* [x] Filter trajectories using organ segmentation (T0 only?)

#### Quality checks
* [ ] Quantify division rates (+ growth rates?) across time
    * [ ] Check that "growth" processes are maintained
* [ ] Check primordia variability: AHP6 level, radial distance, theta
* [ ] Check CLV3 variability (with time)

#### L1 Maps
* [x] Auxin / AHP6 Maps
	* [x] Without time-shift
	* [ ] With time shift? 
* [ ] Moving maps?!?

#### Geometrical Properties
* [ ] Link AHP6 / Curvature in primordia
* [x] r/z profiles of nuclei in primordia direction
	* [x] 3D kymographs R, z, t + AHP6 (with trajectories)
	* [ ] Time axis per primordium / Unique dev. time axis 
* [ ] Shape of AHP6 peak? 
* [ ] Characterize Auxin wave?

#### Clustering
* [ ] Sub-clusters (in primordia clusters?) (using CLV3?)
	* [ ] Using angle / (x, y) to refine clusters
* [ ] Clustering within sectors to better define primordia
	* [ ] Cluster on AHP6 in sectors

#### Auxin Integrals
* [x] AHP6 Derivative vs Auxin? [Hugo]
	* [ ] Estimate derivatives (and integrals?) using B-Spline fit? 
* [x] Auxin integral vs AHP6 (last time point?) vs AHP6 derivative?
	* [ ] Correlations (rank?) in primordia for each time-point 
* [ ] Incrementally build a map of initial Integrated Auxin value at T0 
* [x] Use developmental time to compute (initial) integral at primordium-level
	* [ ] As a function of relative (r, theta) coordinates
	* [ ] Using helical periodicity for computing time shift
	* [ ] Use more than 1 individual to compute initial values

#### Temporal integration
* [x] Filter cells (primordia / PZ)
* [ ] Try with another definition of Auxin?

$$ A = \frac{1}{1 + \mathrm{qDII}}$$

$$ A = -\log(\mathrm{qDII})$$

* [ ] Fit typical evolution equation + measure MSE
	* [ ] Incremental of more and more complex functions
	* [ ] With parameter penalization

$$ \frac{\mathrm{d} R}{\mathrm{d} t} = \alpha \left( A - A_{\mathrm{tr}} \right)_+ - \delta R $$

$$ \alpha = \begin{cases} \alpha\left(r\right) &\mathrm{if} \int_{t_0}^{t} (A - A_{\mathrm{int}})_+ \mathrm{d}u > A_\min \\\ 0 & \mathrm{otherwise.} \end{cases} $$

$$ \alpha \left( r \right) = \frac{\alpha_0}{1 + e^{- \lambda (r - r_0)}} $$

* [ ] Fit equations on pseudo-nuclei (from moving maps) to limit effect of noise

#### Primordia Definition
* [x] AHP6 Maps -> Local maxima -> Radius around?
* [ ] Same with Auxin maps!
* [x] Number of cells around AHP6 maxima -> how many to remain homogeneous?
* [ ] "Lagrangian" perspective sitting on the AHP6 maximum

#### PCC
* [x] AHP6 Maps -> Local maxima -> Closest trajectory
* [x] Relative trajectory (r, theta) coordinates (PCC or AHP6 max)
* [ ] C,L,R,P,D Trajectories:
	* [ ] closest to reference (r, theta) positions
	* [ ] closest (non-sibling) to the PCC in reference directions

#### Machine Learning
* [x] Is auxin sufficient to predict AHP6? (intuitively: no)
    * [ ] Parameter exploration for best performance
	* [ ] Additional variables -> improvement?
* [ ] Visualize "learned functions" on variable subspaces

#### Pseudo-trajectories
* [ ] UMAP on (sub-)trajectories? [Hugo]
* [ ] Other dimensionality reduction approaches? ([MDS](https://scikit-learn.org/stable/modules/generated/sklearn.manifold.MDS.html)?)