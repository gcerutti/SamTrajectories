from typing import Tuple

import numpy as np
from numpy.typing import NDArray

from skimage.transform import rescale

from timagetk import SpatialImage

from maps_2d.epidermal_maps import compute_local_2d_signal


def _slice_coords_from_points(
    points: NDArray[float], img: SpatialImage, downscale_level: int=3
) -> Tuple[NDArray[float]]:
    """
    Compute 3D coordinates of a curved slice in an image based on 3D points.

    This function computes 3D slice coordinates based on provided 3D points,
    adjusting for the image's voxel size and dimensions. It first generates a
    downscaled XY grid of the image, computes local Z grid based on the 3D
    points, and then rescales and adjusts these values to determine the
    corresponding 3D coordinates on the image.

    Parameters
    ----------
    points : NDArray[float]
        A 2D array where each row represents a point with xyz coordinates.
    img : SpatialImage
        The 3D image to slice.
    downscale_level : int
        The level of downscaling applied to the x and y dimensions for
        computational efficiency, by default 3.

    Returns
    -------
    NDArray[float]
        A 2D array of integer coordinates representing the sliced positions
        in the image volume.
    """
    assert img.is3D()

    x_range = np.arange(img.shape[2])*img.voxelsize[2]
    y_range = np.arange(img.shape[1])*img.voxelsize[1]

    xx, yy = np.meshgrid(x_range, y_range, indexing='ij')
    downscale_factor = np.power(2, downscale_level)

    downscaled_x_range = x_range[::downscale_factor]
    downscaled_y_range = y_range[::downscale_factor]

    downscaled_xx, downscaled_yy = np.meshgrid(
        downscaled_x_range, downscaled_y_range, indexing='ij'
    )
    downscaled_zz = compute_local_2d_signal(
        positions=points[:, :2],
        points=np.transpose([downscaled_xx, downscaled_yy], (1, 2, 0)),
        signal_values=points[:, 2]
    )

    zz = rescale(downscaled_zz, downscale_factor)[:len(y_range), :len(x_range)]

    coords = np.transpose([zz, yy, xx],(1, 2, 0))/np.array(img.voxelsize)

    coords = np.maximum(np.minimum(coords, np.array(img.shape) - 1), 0)
    coords = np.nan_to_num(coords)
    coords = np.round(coords).astype(int)

    return tuple(np.transpose(np.concatenate(coords)))

