import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from timagetk.io import imread

from trajectory_analysis import filter_trajectories, interpolate_trajectories
from l1_slice import _slice_coords_from_points


if __name__ == "__main__":
    # project_path = "/Users/gcerutti/Projects/RDP/HugoNucleiTracking"
    project_path = "/projects/SamTime"

    experiment_name = "AHP6 qDII CLV long kinetic REP2"

    # microscopy_dirname = f"{project_path}/data/{experiment_name}/"
    microscopy_dirname = f"{project_path}/microscopy/{experiment_name}/"
    output_dirname = f"{project_path}/results/{experiment_name}/"

    filenames = []
    filenames += ["AHP6 qDII CLV SAM1"]
    filenames += ["AHP6 qDII CLV SAM3"]
    filenames += ["AHP6 qDII CLV SAM7"]
    filenames += ["AHP6 qDII CLV SAM11"]

    hour_times = [0, 6, 12, 18, 24, 30, 36]

    r_max = 65
    length_min = 6

    nuclei_data = {}
    trajectory_data = {}
    interpolated_trajectory_data = {}
    for filename in filenames:
        _nuclei_data = []
        for time in hour_times:
            nuclei_filename = f"{output_dirname}/{filename}/aligned_nuclei_data/{filename}-T{time:02d}_aligned_nuclei_data.csv"
            nuclei_df = pd.read_csv(nuclei_filename)
            nuclei_df['hour_time'] = time
            _nuclei_data.append(nuclei_df)
        nuclei_df = pd.concat(_nuclei_data)
        nuclei_data[filename] = nuclei_df

        trajectory_filename = f"{output_dirname}/{filename}/aligned_nuclei_data/{filename}_aligned_nuclei_trajectory_data.csv"
        trajectory_df = pd.read_csv(trajectory_filename)

        trajectory_df['hour_time'] = [hour_times[time] for time in trajectory_df['time']]
        trajectory_df['Auxin'] = 1 - trajectory_df['qDII']
        trajectory_df['log_qDII'] = np.log(trajectory_df['qDII'])
        trajectory_df['log_AHP6'] = np.log(trajectory_df['AHP6'])
        trajectory_df['log_CLV3'] = np.log(trajectory_df['CLV3'])
        trajectory_data[filename] = trajectory_df

        filtered_trajectory_df = filter_trajectories(trajectory_df, l1_only=True, r_max=r_max, length_min=length_min)
        interpolated_trajectory_df = interpolate_trajectories(filtered_trajectory_df)
        interpolated_trajectory_data[filename] = interpolated_trajectory_df


    for filename in filenames:

        figure = plt.figure(figsize=(10*len(hour_times), 10))
        axes = figure.subplots(1, len(hour_times))

        for time, ax in zip(hour_times, axes):
            print(f"{filename}-T{time:02d}")

            image_filename = f"{microscopy_dirname}/{filename}-T{time:02d}.czi"
            img = imread(image_filename, channel_names=["DIIV", "AHP6", "TagBFP", "CLV3"])

            time_nuclei_df = nuclei_data[filename].query(f"hour_time=={time} & layer==1")
            time_trajectory_df = trajectory_data[filename].query(f"hour_time=={time} & layer==1")
            time_interpolated_trajectory_df = interpolated_trajectory_data[filename].query(f"hour_time=={time}")

            nuclei_points = time_nuclei_df[[f'center_{dim}' for dim in 'xyz']].values

            coords = _slice_coords_from_points(
                nuclei_points, img, downscale_level=3
            )

            l1_slice_img = np.mean([
                img[c].get_array()[coords].reshape(img.shape[1:])
                for c in img.channel_names
            ], axis=0)

            extent = (
                (-1/2)*img.voxelsize[2], (img.shape[2]-1/2)*img.voxelsize[2],
                (img.shape[1]-1/2)*img.voxelsize[1], (-1/2)*img.voxelsize[1]
            )
            img_center = (np.array(img.shape)-1)*np.array(img.voxelsize)/2

            ax.imshow(
                l1_slice_img.T, extent=extent,
                cmap='gray', vmin=0, vmax=10000
            )

            ax.scatter(
                time_nuclei_df['center_x'].values,
                time_nuclei_df['center_y'].values,
                fc='none', ec='r', s=40
            )

            ax.scatter(
                time_trajectory_df['center_x'].values,
                time_trajectory_df['center_y'].values,
                fc='none', ec='orange', s=40
            )

            time_interpolated_trajectory_real_df = time_interpolated_trajectory_df.query('interpolated==0')
            ax.scatter(
                time_interpolated_trajectory_real_df['center_x'].values,
                time_interpolated_trajectory_real_df['center_y'].values,
                fc='none', ec='limegreen', s=40
            )

            time_interpolated_trajectory_created_df = time_interpolated_trajectory_df.query('interpolated==1')
            ax.scatter(
                time_interpolated_trajectory_created_df['center_x'].values,
                time_interpolated_trajectory_created_df['center_y'].values,
                fc='none', ec='turquoise', s=40
            )

            ax.set_xlim(img_center[2]-80, img_center[2]+80)
            ax.set_ylim(img_center[1]-80, img_center[1]+80)

        figure.tight_layout()
        figure_filename = f"{output_dirname}/{filename}/aligned_nuclei_data/{filename}_nuclei_trajectory_interpolation.png"
        figure.savefig(figure_filename)
