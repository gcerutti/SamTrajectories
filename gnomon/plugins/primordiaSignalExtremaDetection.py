# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_bool, d_real, d_inliststring, d_inliststringlist

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, dataFrameOutput, pointCloudOutput
# #}
# add your imports before the next gnomon tag

import logging
from copy import deepcopy

import numpy as np
import pandas as pd

from maps_2d.signal_map import SignalMap
from maps_2d.signal_map_analysis import signal_map_landscape_analysis, compute_signal_map_landscape
from maps_2d.epidermal_maps import compute_local_2d_signal

from sam_atlas.utils import golden_angle

# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Primordia Signal Maxima")
@pointCloudInput("nuclei_df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput("primordia_df", data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('primordia_data_df', data_plugin="gnomonDataFrameDataPandas")
class primordiaSignalExtremaDetection(gnomon.core.gnomonAbstractFormAlgorithm):
    """Detect signal maxima per primordium assuming a regular spiral phyllotaxis

    """

    def __init__(self):
        super().__init__()

        self._parameters = {
            'signal_name': d_inliststring("", [""], "Signal for which to detect local maxima"),
            'cell_radius': d_real("Cell radius", 5., 0, 100, 1, "Radius used for the 2D map generation"),
            'density_k': d_real("Sharpness", 0.35, 0, 10, 2, "Sharpness used for the 2D map generation"),
            'extremum_types': d_inliststringlist("Extremum types", ['maximum'], ['maximum', 'minimum'], "Types of extrema to detect"),
            'filter_organs': d_bool("Filter organs", False, "Whether to filter out outer organs when computing the map")
        }

        self.nuclei_df = {}
        self.primordia_df = {}
        self.primordia_data_df = {}

    def refreshParameters(self):
        if len(self.nuclei_df) > 0:
            nuclei_df = list(self.nuclei_df.values())[0]

            scalar_columns = [v for v in nuclei_df.columns if not 'Unnamed:' in v]
            scalar_columns = [v for v in scalar_columns if not np.array(nuclei_df[v]).dtype == np.dtype('O')]
            scalar_columns = [v for v in scalar_columns if np.array(nuclei_df[v]).ndim == 1]
            signal_name = self['signal_name']
            self._parameters['signal_name'].setValues(scalar_columns)
            if len(scalar_columns)>0:
                if signal_name in scalar_columns:
                    self._parameters['signal_name'].setValue(signal_name)
                else:
                    self._parameters['signal_name'].setValue(scalar_columns[0])

    def run(self):
        self.primordia_df = {}
        self.primordia_data_df = {}
        # #}
        # implement the run method

        signal_name = self['signal_name']
        primordium_range = [-3, -2, -1, 0, 1, 2]

        clv3_radius = 28.
        opening_angle = 30.
        plastochron = 12.

        # sector_max_angle = lambda t: opening_angle
        sector_max_angle = lambda t: opening_angle + 10*(np.maximum(0, 1-t))
        sector_radius_min = lambda t: (0.75 + 0.2*t + 0.1*np.maximum(0, -t))*clv3_radius
        # sector_radius_min = lambda t: (0.85 + 0.2*t + 0.15*np.maximum(0, -t))*clv3_radius
        sector_radius_max = lambda t: (1.95 + 0.25*t + 0.05*np.maximum(0, -t))*clv3_radius
        # sector_radius_max = lambda t: (1.65 + 0.25*t + 0.1*np.maximum(0, -t))*clv3_radius

        for time in self.nuclei_df.keys():
            nuclei_df = self.nuclei_df[time]
            nuclei_df = nuclei_df.query('layer == 1').copy()
            if self['filter_organs']:
                if 'organ' in nuclei_df.columns:
                    logging.info(f"Removing outer organs: {len(nuclei_df)} -> {(nuclei_df['organ']==0).sum()} nuclei")
                    nuclei_df = nuclei_df.query('organ == 0').copy()
                else:
                    logging.warning("No organ column found, impossible to remove outer organs")

            signal_map = SignalMap(
                nuclei_df, position_name='aligned', extent=100, resolution=1,
                radius=self['cell_radius'], density_k=self['density_k']
            )
            signal_map.compute_signal_map(signal_name)

            compute_signal_map_landscape(signal_map, [signal_name])
            extrema_df = signal_map_landscape_analysis(signal_map, signal_name)

            primordium_extrema_data = []

            if 'maximum' in self['extremum_types']:
                maxima_df = extrema_df.query("extremum_type == 'maximum' & extremality > 0.2").copy(deep=True)
                maxima_df['score'] = maxima_df[signal_name] - nuclei_df[signal_name].min()
                primordium_maxima_df = pd.DataFrame(columns=maxima_df.columns)
                for primordium in primordium_range:
                    primordium_theta = (primordium*np.degrees(golden_angle) + 180)%360 - 180
                    primordium_time = primordium + time/plastochron
    
                    max_angle = sector_max_angle(primordium_time)
                    radius_min = sector_radius_min(primordium_time)
                    radius_max = sector_radius_max(primordium_time)
    
                    p_df = maxima_df.copy(deep=True)
                    p_df['score'] *= np.abs((p_df['aligned_theta']-primordium_theta+180)%360-180) < max_angle/2
                    p_df['score'] *= p_df['radial_distance'] > radius_min
                    p_df['score'] *= p_df['radial_distance'] < radius_max
                    if p_df['score'].max() > 0:
                        p_max_df = p_df.iloc[np.argmax(p_df['score'])].copy(deep=True)
                        primordium_maxima_df.loc[primordium] = p_max_df
                    else:
                        primordium_maxima_df.loc[primordium] = np.nan
    
                primordium_maxima_df['primordium'] = primordium_maxima_df.index
                primordium_extrema_data.append(primordium_maxima_df)

            if 'minimum' in self['extremum_types']:
                minima_df = extrema_df.query("extremum_type == 'minimum' & extremality > 0.2").copy(deep=True)
                minima_df['score'] = nuclei_df[signal_name].max() - minima_df[signal_name]
                primordium_minima_df = pd.DataFrame(columns=minima_df.columns)
                for primordium in primordium_range:
                    primordium_theta = (primordium*np.degrees(golden_angle) + 180)%360 - 180
                    primordium_time = primordium + time/plastochron

                    max_angle = sector_max_angle(primordium_time)
                    radius_min = sector_radius_min(primordium_time)
                    radius_max = sector_radius_max(primordium_time)

                    p_df = minima_df.copy(deep=True)
                    p_df['score'] *= np.abs((p_df['aligned_theta']-primordium_theta+180)%360-180) < max_angle/2
                    p_df['score'] *= p_df['radial_distance'] > radius_min
                    p_df['score'] *= p_df['radial_distance'] < radius_max

                    if p_df['score'].max() > 0:
                        p_min_df = p_df.iloc[np.argmax(p_df['score'])].copy(deep=True)
                        primordium_minima_df.loc[primordium] = p_min_df
                    else:
                        primordium_minima_df.loc[primordium] = np.nan

                primordium_minima_df['primordium'] = primordium_minima_df.index
                primordium_extrema_data.append(primordium_minima_df)

            primordium_extrema_df = pd.concat(primordium_extrema_data)
            for field in ['aligned_z']:
                if field in nuclei_df.columns:
                    primordium_extrema_df[field] = compute_local_2d_signal(
                        nuclei_df[[f'aligned_{dim}' for dim in 'xy']].values,
                        primordium_extrema_df[[f'aligned_{dim}' for dim in 'xy']].values.astype(float),
                        nuclei_df[field].values,
                        cell_radius=self['cell_radius'], density_k=self['density_k']
                    )
            primordium_extrema_df.reset_index(inplace=True, drop=True)

            self.primordia_df[time] = primordium_extrema_df

        for time in self.primordia_df.keys():
            self.primordia_data_df[time] = deepcopy(self.primordia_df[time])